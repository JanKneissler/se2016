package environmentalObjects;

import java.awt.Color;
import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class Poison extends EnvObject {

	public static double colorIndex = Colors.getIndexByDisplayColor(Color.RED);
	public static DiskType poisonType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "Poison";
	
	public Poison(Environment env, Random envRandom, double radius) {
		super(env, envRandom, poisonType, colorIndex, name, radius);
	}

}
