package environmentalObjects;

import java.util.Random;

import diskworld.Environment;

public class Area extends EnvObject {
	
	private static final String name = "Area";
	private static double radius = 48;
	
	public Area(Environment env, Random envRandom, double colorIndex) {
		super(env, envRandom, colorIndex, name, radius);
	}
	
	public static double getRadius() {
		return radius;
	}
	
}
