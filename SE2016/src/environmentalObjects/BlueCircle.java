package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class BlueCircle extends EnvObject {

	public static double colorIndex = -0.1; //Colors.getIndexByDisplayColor(Colors.MATTE_BLUE); //-0.1;
	public static DiskType blueCircleType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "BlueCircle";
	
	public BlueCircle(Environment env, Random envRandom) {
		this(env, envRandom, 15);
	}

	public BlueCircle(Environment env, Random envRandom, double radius) {
		super(env, envRandom, blueCircleType, colorIndex, name, radius);
	}
	
}
