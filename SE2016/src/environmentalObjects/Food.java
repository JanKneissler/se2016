package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class Food extends EnvObject {
	
	public static double colorIndex = Colors.getIndexByDisplayColor(Colors.MATTE_GREEN);
	public static DiskType foodType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "Food";

	public Food(Environment env, Random envRandom, double radius) {
		super(env, envRandom, foodType, colorIndex, name, radius);
	}


}
