package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class GreenSquare extends EnvObject {

	public static double colorIndex = 0.2;
	public static DiskType greenSquareType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "GreenSquare";

	public GreenSquare(Environment env, Random envRandom) {
		this(env, envRandom, 10);
	}
	
	public GreenSquare(Environment env, Random envRandom, double radius) {
		super(env, envRandom, greenSquareType, colorIndex, name, radius);
		this.addNewItem(0, Math.toRadians(90), 0, radius, greenSquareType);
		this.addNewItem(0, 0, 0, radius, greenSquareType);
		this.addNewItem(-1, Math.toRadians(90), 0, radius, greenSquareType);
	}

}
