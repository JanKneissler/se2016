package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class RedTriangle extends EnvObject {
	
	public static double colorIndex = -0.5;
	public static DiskType redTriangleType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "RedTriangle";

	public RedTriangle(Environment env, Random envRandom) {
		this(env, envRandom, 10);
	}

	public RedTriangle(Environment env, Random envRandom, double radius) {
		super(env, envRandom, redTriangleType, colorIndex, name, radius);
		this.addNewItem(0, Math.toRadians(0), 0, radius, redTriangleType);
		this.addNewItem(0, Math.toRadians(60), 0, radius, redTriangleType);
	}
	
}
