package environmentalObjects;

import java.util.Random;

import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.DiskComplex;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import utils.Colors;

/**
 * general Object in Environment
 */
public class EnvObject extends ObjectConstructor {
	
	private DiskType diskType;
	private Environment environment;
	private Random envRandom;
	private int size = 1;
	private double colorIndex;
	private String name;
	
	/**
	 * Constructor for already generated diskType
	 */
	public EnvObject(Environment env, Random envRandom, DiskType diskType, double colorIndex, String name, double radius) {
		super(env);
		this.environment = env;
		this.envRandom = envRandom;
		this.diskType = diskType;
		this.colorIndex = colorIndex;
		this.name = name;
		this.setRoot(radius, false, diskType);
	}
	
	/**
	 * Constructor for not already generated diskType (i.e. Bucket, Area)
	 */
	public EnvObject(Environment env, Random envRandom, double colorIndex, String name, double radius) {
		super(env);
		this.environment = env;
		this.envRandom = envRandom;
		this.diskType = new DiskType(new Colors(1, 0.01, 1, 1, colorIndex));
		this.colorIndex = colorIndex;
		this.name = name;
		this.setRoot(radius, false, diskType);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* createObject                                                                          */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	// random positioning
	public boolean createObject() {
		double posx = ScenarioUtils.uniform(envRandom, 0.0, environment.getMaxX());
		double posy = ScenarioUtils.uniform(envRandom, 0.0, environment.getMaxY());
		double orientation = 0.0; 
		double scale = 1.0; //ScenarioUtils.uniform(envRandom, 1, 2);
		return createDiskComplex(posx, posy, orientation, scale) != null;
	}
	
	// random positioning, returning DiskComplex
	public DiskComplex createDiskComplex() {
		double posx = ScenarioUtils.uniform(envRandom, 0.0, environment.getMaxX());
		double posy = ScenarioUtils.uniform(envRandom, 0.0, environment.getMaxY());
		double orientation = 0.0; 
		double scale = 1.0; //ScenarioUtils.uniform(envRandom, 1, 2);
		return createDiskComplex(posx, posy, orientation, scale);
	}
	
	// overload: for non random positioning
	public boolean createObject(double posx, double posy, double scale) {
		double orientation = 0.0;
		return createDiskComplex(posx, posy, orientation, scale) != null;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* other methods                                                                         */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	// use addNewItem instead of addItem to keep track of size
	protected int addNewItem(int parentIndex, double positionAngle, int rotationAngle, double radius, DiskType diskType) {
		this.size++;
		return this.addItem(parentIndex, positionAngle, rotationAngle, radius, diskType);
	}
	

	// Getters
	public DiskType getDiskType() {
		return diskType;
	}

	public int getSize() {
		return size;
	}
	
	public double getColorIndex() {
		return colorIndex;
	}
	
	public String getName() {
		return name;
	}

	
}
