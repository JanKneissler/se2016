package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class YellowStar extends EnvObject {

	public static double colorIndex = 0.9;
	public static DiskType yellowStarType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "YellowStar";
	
	public YellowStar(Environment env, Random envRandom) {
		this(env, envRandom, 7, 4, 3);
	}

	public YellowStar(Environment env, Random envRandom, double radius, double innerPointRad, double outerPointRad) {
		super(env, envRandom, yellowStarType, colorIndex, name, radius);
		
		this.addNewItem(0, Math.toRadians(0), 0, innerPointRad, yellowStarType);
		this.addNewItem(-1, Math.toRadians(0), 0, outerPointRad, yellowStarType);
		
		this.addNewItem(0, Math.toRadians(72), 0, innerPointRad, yellowStarType);
		this.addNewItem(-1, Math.toRadians(0), 0, outerPointRad, yellowStarType);
		
		this.addNewItem(0, Math.toRadians(144), 0, innerPointRad, yellowStarType);
		this.addNewItem(-1, Math.toRadians(0), 0, outerPointRad, yellowStarType);
		
		this.addNewItem(0, Math.toRadians(216), 0, innerPointRad, yellowStarType);
		this.addNewItem(-1, Math.toRadians(0), 0, outerPointRad, yellowStarType);
		
		this.addNewItem(0, Math.toRadians(288), 0, innerPointRad, yellowStarType);
		this.addNewItem(-1, Math.toRadians(0), 0, outerPointRad, yellowStarType);
		
	}
	
}
