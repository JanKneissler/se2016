package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class PinkCorner extends EnvObject {

	public static double colorIndex = -0.89;
	public static DiskType pinkCornerType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "PinkCorner";

	public PinkCorner(Environment env, Random envRandom) {
		this(env, envRandom, 7);
	}
	
	public PinkCorner(Environment env, Random envRandom, double radius) {
		super(env, envRandom, pinkCornerType, colorIndex, name, radius);
		
		this.addNewItem(0, Math.toRadians(0), 0, radius, pinkCornerType);
		this.addNewItem(-1, Math.toRadians(0), 0, radius, pinkCornerType);
		this.addNewItem(0, Math.toRadians(90), 0, radius, pinkCornerType);
		this.addNewItem(-1, Math.toRadians(0), 0, radius, pinkCornerType);
	}

}
