package environmentalObjects;

import java.util.Random;
import diskworld.Environment;

public class Bucket extends EnvObject {
	
	private static final String name = "Bucket";
	private static double radius = 48;

	public Bucket(Environment env, Random envRandom, double colorIndex) {
		super(env, envRandom, colorIndex, name, radius);
	}
	
	public static double getRadius() {
		return radius;
	}
	
}
