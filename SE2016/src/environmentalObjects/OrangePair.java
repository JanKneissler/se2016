package environmentalObjects;

import java.util.Random;

import diskworld.DiskType;
import diskworld.Environment;
import utils.Colors;

public class OrangePair extends EnvObject {

	public static double colorIndex = 0.6;
	public static DiskType orangePairType = new DiskType(new Colors(1.0, 1.0, 2500, 1, colorIndex));
	private static final String name = "OrangePair";
	
	public OrangePair(Environment env, Random envRandom) {
		this(env, envRandom, 10);
	}
	
	public OrangePair(Environment env, Random envRandom, double radius) {
		super(env, envRandom, orangePairType, colorIndex, name, radius);
		this.addNewItem(0, Math.toRadians(90), 0, radius, orangePairType);
	}
	
}
