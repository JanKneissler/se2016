package games;

import agent.Agent;
import diskworld.DiskMaterial;
import diskworld.Environment;
import diskworld.interfaces.Actuator;

/**
 * Blocker-Agent
 * six discs arranged to a rectangle
 */
public class Blocker extends Agent {
	
	double radius; 
	
	public Blocker(Environment env, Actuator actuator, DiskMaterial bodyMaterial, double radius) {
		super(env, actuator, bodyMaterial, radius);
		this.createDiskType("blocker", bodyMaterial);

		this.addNewItem(0, Math.toRadians(90), 0, radius, "blocker");
		this.addNewItem(0, Math.toRadians(0), 0, radius, "blocker");
		this.addNewItem(-1, Math.toRadians(90), 0, radius, "blocker");
		this.addNewItem(-2, Math.toRadians(0), 0, radius, "blocker");
		this.addNewItem(-1, Math.toRadians(90), 0, radius, "blocker");
		this.radius = radius; 
	}

	public double getRadius() {
		return radius;
	}



}
