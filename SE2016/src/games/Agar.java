package games;

import java.awt.Color;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actuators.AbsoluteMover;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;


public class Agar extends GlobalInformationFixedDiskNumScenario {

	private static final double MAX_MOVER_DISTANCE = 10; // max_mover_distance_per_coordinate
	private static final double FLOOR_TILE_SIZE = 100; // 10 x 10 floor tiles
	private int missedResizes;
	private Disk foodDisk,fingerDisk;
	private Random envRandom;
	private boolean couldResize;

	public Agar() {
		super(2, false, false, FLOOR_TILE_SIZE);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "Agar"; // darf nicht null sein
	}

	@Override
	public String getDescription() {
		return "In this scenario, the agent can increase its size when touching other disks"+
				"Whenever it touches other disk, this disk disappears and positive reward is given.";
	}

	@Override
	public String getAuthors() {
		return "Fabian";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		envRandom = random;
		
		environment.getPhysicsParameters().setGravityModel(new ConstantGravityModel(100.0));
		
		// Initialize the floor
		int mouthx = ScenarioUtils.uniform(random, 0, environment.getFloor().getNumX()-1);
		int mouthy = ScenarioUtils.uniform(random, 0, environment.getFloor().getNumY()-1);
		Floor floor = environment.getFloor();
		floor.fill(FloorCellType.GRASS);

		// create disk types for objects 
		DiskType foodType = new DiskType(DiskMaterial.RUBBER);

		// create objects (DiskComplexes that are not agents)

		ObjectConstructor foodConstructor = environment.createObjectConstructor();
		foodConstructor.setRoot(10, foodType);

		boolean wasPlaced = false;
		do {
			// random position and scale 
			double posx = ScenarioUtils.uniform(random, 0.0, getEnvironment().getMaxX());
			double posy = ScenarioUtils.uniform(random, 0.0, getEnvironment().getMaxY());
			double orientation = 0.0; // orientation dioes not matter
			double scale = ScenarioUtils.uniform(random, 1.0, 2.0);
			wasPlaced = foodConstructor.createObject(posx, posy, orientation, scale);
		} while(!wasPlaced);

		// set disk #0
		DiskComplex dc = environment.getDiskComplexes().iterator().next();
		foodDisk = dc.getDisks().get(0);
		setDisk(0, foodDisk);
		
		// set shift resistance energy consumption to 0!
		environment.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.0);
	};

	
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {
		// sensors and actuators
		Actuator actuator = new AbsoluteMover(MAX_MOVER_DISTANCE, 0.0);
		// create disk types 
		DiskType body = new DiskType(DiskMaterial.RUBBER.withColor(Color.YELLOW), actuator);

		// now create constructor specifying relative arrangement of disks
		AgentConstructor ac = new AgentConstructor(getEnvironment());
		ac.createDiskType("body", DiskMaterial.RUBBER.withColor(Color.YELLOW));
		ac.setRoot(20, body);
		// now repeat creating the agent at a random place
		ControlledAgentData agent = null;
		// random scale factor controlling the size (using the random object for agent)
		double scaleFactor = 1.0;
		double initialEnergyLevel = 0.5; // start with half energy
		controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
		do {
			// random position and orientation (using the random object for environment)
			double posx = ScenarioUtils.uniform(eRandom, 0.0, getEnvironment().getMaxX());
			double posy = ScenarioUtils.uniform(eRandom, 0.0, getEnvironment().getMaxY());
			double orientation = 0.0; // orientation dioes not matter
			agent = ac.createControlledAgent(posx, posy, orientation, scaleFactor);
		} while (agent == null);
		// set disk #1
		fingerDisk = agent.getDiskComplex().getDisks().get(0);
		final Environment env = getEnvironment();
		setDisk(1, fingerDisk);
		// set eventhandler that glues together when colliding with sth else
		fingerDisk.addEventHandler(new CollisionEventHandler() {

			@Override
			public void collision(final CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				if (otherDisk instanceof Disk) {
					resetPosition((Disk) otherDisk);
					missedResizes++;
					((Disk) otherDisk).getDiskComplex().setVelocity(0, 0);
				}
				
			}
		});
		return (new ControlledAgentData[]{agent});
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		// check if food disk is on food spot
		if (missedResizes > 0) {
			couldResize = getEnvironment().canChangeIsolatedDiskRadius(fingerDisk, fingerDisk.getRadius()*1.1);
			if (couldResize)
				missedResizes--;
			couldResize = false;
		}
	}

	private void resetPosition(Disk d) {
		double posx,posy;
		do {
			// random position and scale
			posx = ScenarioUtils.uniform(envRandom, 0.0, getEnvironment().getMaxX());
			posy = ScenarioUtils.uniform(envRandom, 0.0, getEnvironment().getMaxY());			
		} while(!getEnvironment().canTeleport(d, posx, posy, 0));		
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		return 0;
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			// we create a keyboard controller with three actions:
			// when key '+' is pressed: increment first value in the actuator array (index 0) by 0.01
			// when key '-' is pressed: decrement it by 0.01
			// when key 0 is pressed: set it to 0.0

			//always(0, 0.0);
			//always(1, 0.0);
			onKey('A', 0, -.1, true);
			onKey('D', 0, +.1, true);
			onKey('W', 1, .1, true);
			onKey('X', 1, -.1, true);

			// Steuerung für zusätzliches Objekt
			//onKey(keyChar, Index of actuator, movement per time step, true: increment)

			setLogMessage("Use the following keys to control the agent:\n" +
					"a,d -> left/right\n" +
					"w,x -> up/down\n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(Agar.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}


}
