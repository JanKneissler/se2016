package games;

import java.awt.Color;
import java.util.Random;

import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actuators.AbsoluteMover;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;

public class Shufflepuck extends GlobalInformationFixedDiskNumScenario {

	private static final double FLOOR_TILE_SIZE = 100;
	private static final double ENERGY_GAIN_PER_GOAL = 0.05;
	private static final double MAX_MOVER_DISTANCE = 10;
	private Disk blockerDisk, puckDisk;
	private Random envRandom;
	boolean once = true;

	/* Constructor */
	public Shufflepuck() {
		super(7, false, false, FLOOR_TILE_SIZE);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* general scenario Information */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "Shufflepuck";
	}

	@Override
	public String getDescription() {
		return "This scenario emulates a simplified version of the game of shufflepuck. \n"
				+ "The blockerDisk moves left an right and tries to block a puckDisk.";
	}

	@Override
	public String getAuthors() {
		return "Tamar";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Initialize the environment, create controlled and npc agents */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		envRandom = random;
		// initialize floor
		Floor floor = environment.getFloor();
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, FloorCellType.ICE);
			}
		}

		DiskType puckType = new DiskType(DiskMaterial.METAL.withColor(Color.ORANGE));

		// object constructor
		ObjectConstructor puckConstructor = environment.createObjectConstructor();
		puckConstructor.setRoot(20, puckType);

		// place object
		boolean wasPlaced = false;
		do {
			// random position
			double posx = ScenarioUtils.uniform(random, 300.0, getEnvironment().getMaxX() - 300);
			double posy = ScenarioUtils.uniform(random, 500.0, getEnvironment().getMaxY());
			double orientation = 0.0;
			double scale = ScenarioUtils.uniform(random, 1, 2);
			wasPlaced = puckConstructor.createObject(posx, posy, orientation, scale);
		} while (!wasPlaced);
		// set disk
		DiskComplex dc = environment.getDiskComplexes().iterator().next();
		puckDisk = dc.getDisks().get(0);
		setDisk(0, puckDisk);
		dc.applyImpulse(0.0, // impulseX: x component of the impulse
				-1000000, // impulseY: y component of the impulse
				0.0); // angularImpulse: change of angular momentum

		/* Set shiftResistance for joints and gravity model */
		environment.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.0);
		environment.getPhysicsParameters().setGravityModel(new ConstantGravityModel(0.0));

	}

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom,
			AgentState[] controlledAgentStates) {
		final Environment env = getEnvironment();

		// sensors and actuators
		Actuator actuator = new AbsoluteMover(MAX_MOVER_DISTANCE, 0.0);

		// create DiskType and build agent-complex
		Blocker blocker = new Blocker(env, actuator, DiskMaterial.RUBBER.withColor(Color.BLUE), 20);

		// place agent
		ControlledAgentData agent = null;
		double scaleFactor = 1.0;
		double initialEnergyLevel = 0.5;
		controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
		do {
			double orientation = 0.0;
			agent = blocker.createControlledAgent(500, 30, orientation, scaleFactor);
		} while (agent == null);

		for (int i = 0; i < blocker.getSize(); i++) {
			blockerDisk = agent.getDiskComplex().getDisks().get(i);
			setDisk(i + 1, blockerDisk);
			blockerDisk.addEventHandler(new CollisionEventHandler() {
				@Override
				public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
					if (otherDisk instanceof Disk) {
						controlledAgentStates[0].incEnergyGained(ENERGY_GAIN_PER_GOAL);
						((Disk) otherDisk).applyImpulse(0, 1000000);
						// absto�en des pucks nach collision
					}
				}
			});
		}

		return new ControlledAgentData[] { agent };
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Called every time step, put necessary update code (besides the
	 * DiskWorldPhysics)
	 */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		if (blockerMissedPuck()) {
			resetPosition(puckDisk);
		}
	}

	private boolean blockerMissedPuck() {
		double lowestYcoordPuck = puckDisk.getY() - puckDisk.getRadius();
		// blocker height is 2 * radius, so the puck is definitly missed if
		// puck's y coord < radius of a blocker disk 
		double lowestYcoordAllowed = blockerDisk.getRadius();
		return lowestYcoordPuck < lowestYcoordAllowed;
	}

	private void resetPosition(Disk d) {
		double posx, posy;
		do {
			// random position and scale
			posx = ScenarioUtils.uniform(envRandom, 300.0, getEnvironment().getMaxX() - 300);
			posy = ScenarioUtils.uniform(envRandom, 500.0, getEnvironment().getMaxY());
		} while (!getEnvironment().canTeleport(d, posx, posy, 0));
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score */
	// /////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		return 0;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			// onKey(char keyChar, int actuatorIndex, double value, boolean
			// increment);
			onKey('A', 0, -1, false); // decrement first value in actuator array
										// (index 0) by 1.3
			onKey('D', 0, +1, false);

			setLogMessage(
					"Use the following keys to control the agent:\n" + "a -> move left \n" + "d -> move right \n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String[] args) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(Shufflepuck.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
