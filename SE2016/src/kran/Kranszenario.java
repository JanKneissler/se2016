package kran;

import java.util.Iterator;
import java.util.Random;

import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.actions.JointActionType;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actions.JointActionType.ControlType;
import diskworld.actuators.Mover;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.FloorCellType;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;
import utils.Colors;



public class Kranszenario extends GlobalInformationFixedDiskNumScenario {

	private static final double FLOOR_TILE_SIZE = 100; // 10 x 10 floor tiles
	private static final double ENERGY_GAIN_PER_HIT = 0.05;
	private static final double ENERGY_GAIN_PER_WIN = 0.1;
	private static int BALL_COUNT ;

	// Disks to be set
	private Disk[] targetDisks = new Disk[BALL_COUNT]; 
	private Disk agentDisk;  
	private Disk ballDisk;  

	private Random envRandom =  getERandom();

	// Calculation of number of rows
	private double mod;
	private int agentSize;
	private double targetSize;

	// Check for hit targets
	private boolean targetHit;
	private Disk hitDisk;
	private int numberOfHitTargets;

	// Calculations for position of agent and ball Position
	private double agentYPostion ;
	private double ballYPositon;

	int elbowId, elbowId2;

	public Kranszenario() {
		super((BALL_COUNT = 1+ (int)( Math.random() * 8)) + 2, false, false, FLOOR_TILE_SIZE);

	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "Kranszenario"; // 
	}

	@Override
	public String getDescription() {
		return "In this scenario, the agent is a disk with a arm attached to its body. \n"+
				"The agent gets rewarded whenever it releases the ball in his hand,  \n" + 
				"so that the ball hits the disk moving on the ground. \n" ;
	}

	@Override
	public String getAuthors() {
		return "Max";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////


	/* floor and npc-disks */
	@Override
	protected void initialize(Environment environment, Random random) {		
				
		getEnvironment().getFloor().fill(
				new FloorCellType(0.03, Colors.MIAMI_BLUE));

		targetHit = false;
		numberOfHitTargets = 0;

		envRandom = random;

		// Define Y Position of ball and Agent
		ballYPositon = getEnvironment().getMaxY() * 0.6;
		agentYPostion = getEnvironment().getMaxY() * 0.6 + (4.5 /* 4 full and a half Disk*/ * 2 /* two times the radius*/ * 10 /*radius*/ + 30/* Radius of body*/);

		// gravity model
		environment.getPhysicsParameters().setGravityModel(new ConstantGravityModel(100.0));

		// Disk types
		DiskType targetType = new DiskType(new DiskMaterial(0.02, 1, 0.1, 0.1, Colors.DEEP_SEA));
		DiskType ballType = new DiskType(new DiskMaterial(1.0, 0.1, 2500, 0.1, Colors.MIAMI_YELLOW));

		// create objects (DiskComplexes that are not agents)
		ObjectConstructor[] targetConstructor = new ObjectConstructor[BALL_COUNT];
		ObjectConstructor ballConstructor = environment.createObjectConstructor();



		// create targets
		targetSize = 10;

		// calculate number of rows, targets get placed in
		mod = Math.floor((getEnvironment().getMaxY()/2 - targetSize) / (targetSize * 4.1));


		for (int i = 0; i < targetConstructor.length; i++) {
			targetConstructor[i] = environment.createObjectConstructor();
			targetConstructor[i].setRoot(targetSize, targetType);

		}


		for (int j = 0; j < targetConstructor.length; j++) {

			DiskComplex dc = null;
			do {
				double posx = ScenarioUtils.uniform(random, 0.0, getEnvironment().getMaxX());
				double posy = ((j%mod) +1) * targetSize  * 4.1;
				double orientation = 0.0; // orientation does not matter
				double scale = ScenarioUtils.uniform(random, 1.0, 2.0);
				dc = targetConstructor[j].createDiskComplex(posx, posy, orientation, scale);

			} while(dc == null);
			// disable gravity
			dc.setAffectedByGravity(false);

		}

		// create Ball (used to shoot at targets)
		ballConstructor.setRoot(10, ballType);

		boolean wasPlaced = false;
		do {
			// gets placed over depotDisk 
			double depotPosX = ScenarioUtils.uniform(random, (5.5 * 20+3), getEnvironment().getMaxX() - (5.5 * 20+3));

			double orientation = 0.0; // orientation does not matter
			double scale = 1.0;
			wasPlaced = ballConstructor.createObject(depotPosX, ballYPositon, orientation, scale);

		} while(!wasPlaced);



		// set all disks	
		Iterator<DiskComplex> it = environment.getDiskComplexes().iterator();
		DiskComplex dc = null;
		int i = 0;
		int targetIndex = 0;
		Disk workDisk;
		while (it.hasNext()){
			dc = it.next();
			workDisk = dc.getDisks().get(0);
			int randomSpeed = getRandom(envRandom, 4, 13);
			if (workDisk.getDiskType() == targetType){
				targetDisks[targetIndex] = workDisk;
				targetDisks[targetIndex].applyImpulse(10 * randomSpeed  * targetDisks[targetIndex].getMass() * ((int)(-1)^i) , 0);
				setDisk(i, targetDisks[targetIndex++]);

			}
			else{
				ballDisk = workDisk;
				setDisk(i, ballDisk);
				dc.setDiskFixed(ballDisk);
			}
			i++;

		}

		// set eventhandler to detect a collision of the ball with a target
		for (int j = 0; j < targetDisks.length; j++) {
			final int k = j; 
			targetDisks[j].addEventHandler(new CollisionEventHandler() {
				@Override
				public void collision(final CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
					if (otherDisk instanceof Disk) {
						if(((Disk) otherDisk).getDiskType() == ballType){
							targetHit = true;
							hitDisk = targetDisks[k];
							numberOfHitTargets++;
						}
					}
				}
			});

		}

		// set shift resistance energy consumption to 0!
		environment.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.0);

	};

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {

		// Actuator
		double maxBackwardDistance = 2; // maximum distance translated backwards in one time step
		double maxForwardDistance = 2; // maximum distance translated forward in one time step
		double maxRotationValue = 0; // the maximum angular speed/rotation angle by which the disk can rotate (in radian per time unit or radian), (non-negative)
		double rotationEnergyConsumption = 0; // energy consumption for full 360 rotation per mass momentum unit, (non-negative)
		double moveEnergyConsumption = 0; // energy consumption for motion by 1 distance unit and per mass unit, (non-negative)

		Actuator actuator = new Mover(maxBackwardDistance, maxForwardDistance, maxRotationValue, rotationEnergyConsumption, moveEnergyConsumption);



		// DiskTypes for body and limb
		DiskType rootType = new DiskType(DiskMaterial.METAL.withColor(Colors.MIAMI_ORANGE), actuator);
		DiskType limbType = new DiskType( new DiskMaterial(2, 0.1, 0.01, 0.5, Colors.MIAMI_PURPLE));

		AgentConstructor ac = new AgentConstructor(getEnvironment());

		// root of the agent
		ac.setRoot(30, rootType);

		// Arm of the agent
		ac.addItem(0, Math.toRadians(270), 0, 10, limbType);

		// left tong
		ac.createJointType("elbow", DiskMaterial.RUBBER.withColor(Colors.MIAMI_ORANGE), new JointActionType(0.5/* per time unit, not per time step */, ControlType.CHANGE, ActionType.SPIN));

		elbowId = ac.addItem(-1, Math.toRadians(270), 0, 10, "elbow");
		ac.setActionRange(elbowId, ActionType.SPIN, -Math.PI/2, Math.toRadians(5));
		ac.addItem(-1, Math.toRadians(90), 0, 10, limbType);
		for (int i = 0; i < 3; i++) {
			ac.addItem(-1, Math.toRadians(0), 0, 10, limbType);
		}

		// right tong
		elbowId2 = ac.addItem(elbowId -1, Math.toRadians(90), 0, 10, "elbow");
		ac.setActionRange(elbowId2, ActionType.SPIN, Math.toRadians(-5), Math.PI/2);

		ac.addItem(-1, Math.toRadians(270), 0, 10, limbType);
		for (int i = 0; i < 3; i++) {
			ac.addItem(-1, Math.toRadians(0), 0, 10, limbType);
		}

		// now repeat creating the agent at a random place
		ControlledAgentData agent = null;
		// random scale factor controlling the size (using the random object for agent)
		double scaleFactor = 1.0;
		double initialEnergyLevel = 0.5; // start with half energy
		controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
		do {
			// random position and orientation (using the random object for environment)
			double posx = ScenarioUtils.uniform(eRandom, 0.0, getEnvironment().getMaxX());
			double orientation = 0.0; // orientation does not matter
			agent = ac.createControlledAgent(posx, agentYPostion, orientation, scaleFactor);

		} while (agent == null);

		// disable gravity
		agent.getDiskComplex().setAffectedByGravity(false);
		agentDisk = agent.getDiskComplex().getDisks().get(0);
		agentSize = agent.getDiskComplex().getDisks().size();



		setDisk(BALL_COUNT+ 1, agentDisk);
		return new ControlledAgentData[] { agent };

	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	// for every time Step:
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {

		/*
		 * M�glichkeit 2 Fixed aufzul�sen
		 */
		if(agentDisk.getDiskComplex().getDisks().size() > agentSize){
			ballDisk.getDiskComplex().setDiskNotFixed(ballDisk);
			ballDisk.applyImpulse(0, -100); // Apply Impulse, so Ball wont be transiently merged to arm
		}

		// Stop agent from colliding with wall
		if(agentDisk.getX() < (5.5 * 20+3)){
			getEnvironment().canTeleport(agentDisk, (5.5 * 20+3), agentYPostion, 0.0);
		}
		if(agentDisk.getX() > getEnvironment().getFloor().getMaxX() - (5.5 * 20+3) ){
			getEnvironment().canTeleport(agentDisk, getEnvironment().getFloor().getMaxX() - (5.5 * 20+3), agentYPostion, 0.0);
		}
		// Agent changes orientation when colliding with wall => reset its Position
		getEnvironment().canTeleport(agentDisk, agentDisk.getX(), agentYPostion, 0.0);

		for (int i = 0; i < targetDisks.length; i++) {
			if(targetDisks[i].getDiskComplex().getSpeedy() > 0){
				targetDisks[i].getDiskComplex().setVelocity(0, 0);
				resetPosition(targetDisks[i]);
			}
		}



		// check if target was hit
		if(targetHit){
			if(numberOfHitTargets == BALL_COUNT){
				numberOfHitTargets = 0;
				agentState[0].incEnergyGained(ENERGY_GAIN_PER_WIN);
			}
			resetPosition(ballDisk);
			nextEpisode();

			for (int i = 0; i < targetDisks.length; i++) {

				if (targetDisks[i] == hitDisk){
					resetPosition(targetDisks[i]);
					targetHit = false;
					hitDisk = null;
					agentState[0].incEnergyGained(ENERGY_GAIN_PER_HIT);

				}
			}
		}

		// reset position of the ball, if the ball hits the floor
		if(ballDisk.getY() <= (0 + 1.5 * ballDisk.getRadius())){
			resetPosition(ballDisk);
			nextEpisode();
		}
	}

	private void resetPosition(Disk d) {
		double posx,posy;

		if(d.equals(ballDisk)){
			posx = ScenarioUtils.uniform(envRandom, (5.5 * 20+3), getEnvironment().getMaxX() -(5.5 * 20+3));
			posy = ballYPositon;
			do {
				posx = ScenarioUtils.uniform(envRandom, (5.5 * 20+3), getEnvironment().getMaxX() -(5.5 * 20+3));

			} while(!getEnvironment().canTeleport(d, posx, posy, 0));
			ballDisk.getDiskComplex().setVelocity(0, 0);
			ballDisk.getDiskComplex().setDiskFixed(ballDisk);

		}
		// d is targetDisk
		else{
			posx = ScenarioUtils.uniform(envRandom, 0.0, getEnvironment().getMaxX());
			for (int i = 0; i < targetDisks.length; i++) {

				if(d.equals(targetDisks[i])){
					posy = ((i%mod) +1) * targetSize  * 4.1;
					do {
						posx = ScenarioUtils.uniform(envRandom, 0.0, getEnvironment().getMaxX());


					} while(!getEnvironment().canTeleport(d, posx, posy, 0));	
					d.getDiskComplex().setVelocity(0, 0);
					int randomSpeed = getRandom(envRandom, 4, 13);
					d.applyImpulse(10 * randomSpeed  * d.getMass() * ((int)(-1)^i) , 0);


				}

			}
		}


	}

	private static int getRandom(Random rand, int from, int to) {
		return rand.nextInt(to - from + 1) + from;	

	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		// TODO Auto-generated method stub
		return 0;
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {


		@Override
		public void addKeyEvents() {

			//onKey(char keyChar, int actuatorIndex, double value, boolean increment);
			//onKey(char keyChar, int actuatorIndex, double value, boolean increment);
			always(0, 0.0);
			always(1, 0.0);
			always(2, 0.0);

			onKey('K', 0, -0.5, false);
			onKey('L', 0, 0.5, false);
			onKey('N', 1, -0.5, false); 
			onKey('M', 1, 0.5, false);
			onKey('D', 2, 1, false);
			onKey('A', 2, -1, false);


			setLogMessage("Use the following keys to control the agent:\n" +
					"a -> turn left \n" +
					"d -> turn right \n" +
					"k -> open left finger \n" +
					"l -> close left finger \n" +
					"m -> open right finger \n" +
					"n -> close right finger \n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(Kranszenario.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}


}

