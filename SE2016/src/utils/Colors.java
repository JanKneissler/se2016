package utils;

import java.awt.Color;

import scenarios.framework.ColorIndexedDiskMaterial;

public class Colors extends ColorIndexedDiskMaterial {

	/**
	 * Greenish, marine colors
	 */

	public final static Color FRENCH_LILAC = new Color(107, 91, 102);
	public final static Color PEWTER = new Color(144, 164, 153);
	public final static Color HEATHERED_GRAY = new Color(179, 176, 143);

	public final static Color ALGA_GREEN = new Color(50, 205, 50);
	public final static Color DARK_ALGA = new Color(29, 138, 49);
	public final static Color ALGA_JOINT = new Color(10, 80, 30);
	public final static Color DEEP_SEA = new Color(149, 148, 189);

	public final static Color FOGGY_YELLOW = new Color(230, 240, 141);
	public final static Color LIGHT_CORAL = new Color(240, 128, 128);
	
	/**
	 * Miami Vice Sunset
	 */

	public final static Color MIAMI_ORANGE = new Color(244, 130, 68);
	public final static Color MIAMI_YELLOW = new Color(252, 202, 76);
	public final static Color MIAMI_PURPLE = new Color(245, 111, 231);
	public final static Color MIAMI_BLUE = new Color(189, 197, 227);
	
	/**
	 * Matte Colors
	 */

	public final static Color MATTE_BLUE = new Color(91, 192, 235);
	public final static Color MATTE_YELLOW = new Color(253, 231, 76);
	public final static Color MATTE_GREEN = new Color(155, 197, 61);
	public final static Color MATTE_RED = new Color(229, 89, 52);
	public final static Color MATTE_ORANGE = new Color(250, 121, 33);
	
	/**
	 * Pastell Colors
	 */

	public final static Color PASTELL_GREY = new Color(124,120,106);
	public final static Color PASTELL_BLUE = new Color(141,205,193);
	public final static Color PASTELL_GREEN = new Color(211,227,151);
	public final static Color PASTELL_YELLOW = new Color(255,245,195);
	public final static Color PASTELL_ORANGE = new Color(235,110,68);
	public final static Color PASTELL_BROWN = new Color(200, 200, 100);
	
	public Colors(double density, double elasticity, double frictionCoefficient, double gripCoefficient,
			double colorIndex) {
		super(density, elasticity, frictionCoefficient, gripCoefficient, colorIndex);
		// TODO Auto-generated constructor stub
	}

	public static Color getDisplayColorByIndex(double colorIndex) {
		int i = (int) ((colorIndex + 1.0) / 2.0 * 0x00FFFFFF) | 0xFF000000;
		Color c = new Color(i);
		return c;
	}
	
	public static double getIndexByDisplayColor(Color c) {
		int i = c.getRGB();
		return ((double) (i ^ 0xFF000000) / (double) 0x00FFFFFF * 2.0 - 1);
	}

	private static Color lighten(Color c) {
		return mix(c, new Color(255,255,255,255));
	}
	
	private static Color darken(Color c) {
		return unmix(c, new Color(255,255,255,255));
	}

	private static Color mix(Color c, Color d) {
		return new Color(
				(c.getRed() + d.getRed())/2, 
				(c.getGreen() + d.getGreen())/2, 
				(c.getBlue() + d.getBlue())/2,
				(c.getAlpha() + d.getAlpha())/2);
	}
	
	private static Color unmix(Color c, Color d) {
		return new Color(
				Math.max(0,c.getRed() * 2 - d.getRed()), 
				Math.max(0,c.getGreen() * 2 - d.getGreen()), 
				Math.max(0,c.getBlue() * 2 - d.getBlue()),
				Math.max(0,c.getAlpha() * 2 - d.getAlpha()));
	}

}
