package giraffe;

import java.awt.Color;
import java.util.Random;

import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;

import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;
import utils.Colors;



public class GiraffeWorld extends GlobalInformationFixedDiskNumScenario {

	private static final double FLOOR_TILE_SIZE = 100; // 10 x 10 floor tiles
	private static final double ENERGY_GAIN_PER_FOOD_ITEM = 0.002;
	private Disk foodDisk;
	private Disk headDisk;
	private Disk[] giraffeDisks;
	private Disk[] jointDisks;
	
	public GiraffeWorld() {
		super(0, false, false, FLOOR_TILE_SIZE);
		// number of npc objects, diskRadiusSensor, diskColorSensor, FloorTileSize
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* First some information about the scenario and its creator(s) 						 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String getName() {
		return "GiraffeWorld"; // darf nicht null sein
	}

	@Override
	public String getDescription() {
		return "In this scenario, the controller moves a giraffe. \n"+
				"Whenether the giraffe reaches the food in the tree \n"+
				"a reward is given. \n";
	}

	@Override
	public String getAuthors() {
		return "Fabian Mikulasch";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Initialise the environment, create controlled and npc agents       				     */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		Floor floor = environment.getFloor();
		FloorCellType ground = new FloorCellType(1, Colors.PASTELL_BROWN);
		FloorCellType sky = new FloorCellType(1, Colors.PASTELL_BLUE);
		for (int x = 0; x < floor.getNumX(); x++) {
			for (int y = 0; y < floor.getNumY(); y++) {
				floor.setType(x, y, (y == 0) ? ground : sky);
			}
		}

		// create disk types for objects, only colors matter
		DiskType foodType = new DiskType(new DiskMaterial(1, 1, 1, 1, new Color(100,190,130)));
		DiskType leavesType = new DiskType(new DiskMaterial(1, 1, 1, 1, new Color(60,170,80)));
		
		ObjectConstructor foodConstructor = environment.createObjectConstructor();
		
		foodConstructor.setRoot(90, foodType);
		DiskComplex leaves = foodConstructor.createDiskComplex(250, 700, 0, 1);
		foodDisk = leaves.getDisks().get(0);
		leaves.setDiskFixed(foodDisk);
		foodDisk.setZLevel(-3);
		
		for (int i = 0; i < 40; i++) {
			double size = ScenarioUtils.uniform(random, 20, 100);
			foodConstructor.setRoot(size, leavesType);
			double posx = ScenarioUtils.uniform(random, size+1, 300);
			double posy = ScenarioUtils.uniform(random, 500, 999-size);
			leaves = foodConstructor.createDiskComplex(posx, posy, 0, 1);
			leaves.setDiskFixed(0);
			leaves.getDisks().get(0).setZLevel(-4-i);
		}

		environment.getPhysicsParameters().setGravityModel(new ConstantGravityModel(200.0, 3));
		environment.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.000000009);
	};
	
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {
		ControlledAgentData giraffe = null;
		Giraffe g = new Giraffe(getEnvironment());
		giraffe = g.createControlledAgent(600, 150, 0, 2);
		controlledAgentStates[0].resetEnergyLevel(0.5);
		headDisk = giraffe.getDiskComplex().getDisks().get(g.getSize()-1);
		giraffeDisks = new Disk[g.getSize()];
		jointDisks = new Disk[14];
		int n = 0;
		for (int i = 0; i < g.getSize(); i++) {
			giraffeDisks[i] = giraffe.getDiskComplex().getDisks().get(i);
			// save jointDisks via try and error
			try {
				giraffe.getDiskComplex().getDisks().get(i).getDiskType().getJointActions().isEmpty();
				jointDisks[n++] = giraffe.getDiskComplex().getDisks().get(i);
			} catch (NullPointerException e) {}
		}
		setDisks();
		return new ControlledAgentData[] { giraffe };
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called every time step, put necessary update code (besides the DiskWorldPhysics)*/
	///////////////////////////////////////////////////////////////////////////////////////////

	// for every time Step:
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		if (headDisk.overlaps(foodDisk)){
			agentState[0].incEnergyGained(ENERGY_GAIN_PER_FOOD_ITEM);
		}
		if (agentState[0].getEnergyLevel() >= 1){
			// hier müsste man die giraffe reseten, im moment ist keine Möglichkeit bekannt.
			nextEpisode();
		}
	}
	
	private void setDisks() {
		Disk[] d = new Disk[giraffeDisks.length + 1];
		d[0] = foodDisk;
		for (int i = 1; i < d.length; i++) {
			d[i] = giraffeDisks[i-1];
		}
		setDisks(d);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score							 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public double getScore() {
		// TODO Auto-generated method stub
		return 0;
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			// we create a keyboard controller with three actions:
			// when key '+' is pressed: increment first value in the actuator array (index 0) by 0.01
			// when key '-' is pressed: decrement it by 0.01
			// when key 0 is pressed: set it to 0.0
			
			for (int i = 0; i < 16; i++) {

				always(i, 0.0);
			}
			//always(1, 0.0);
			double speed = 0.2;
			onKey('Q', 0, -speed, true);
			onKey('A', 0, +speed, true);
			onKey('W', 1, +speed, true);
			onKey('S', 1, -speed, true);
			onKey('E', 2, -speed, true);
			onKey('D', 2, +speed, true);
			onKey('R', 3, +speed, true);
			onKey('F', 3, -speed, true);
			onKey('T', 4, -speed, true);
			onKey('G', 4, +speed, true);
			onKey('Z', 5, +speed, true);
			onKey('H', 5, -speed, true);
			onKey('U', 6, -speed, true);
			onKey('J', 6, +speed, true);
			onKey('I', 7, +speed, true);
			onKey('K', 7, -speed, true);
			onKey('C', 8, +0.5*speed, true);
			onKey('V', 8, -0.5*speed, true);
			// the entire neck consists of joints
			for (int i = 10; i < 16; i+=2) {
				onKey('Y', i,   +0.5*speed, true);
				onKey('X', i+1, -0.5*speed, true);
			}

			setLogMessage("Use the following keys to control the agent:\n" +
					"q ... i -> close legs / legs to body\n" +
					"a ... k -> stretch out legs / stand up\n" +
					"c, v    -> lift head");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(GiraffeWorld.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}


}
