package giraffe;

import java.awt.Color;

import agent.Agent;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actions.JointActionType;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actions.JointActionType.ControlType;
import utils.Colors;

public class Giraffe extends Agent {

	public Giraffe(Environment env) {
		super(env);
		DiskType body = new DiskType(new DiskMaterial(1, 0.01, 10, 10, Colors.PASTELL_ORANGE));
		this.createDiskType("body", new DiskMaterial(0.1, 0.01, 10, 10, Colors.PASTELL_ORANGE));
		this.createDiskType("head", new DiskMaterial(0, 0.01, 10, 10, Colors.MATTE_YELLOW));
		this.createJointType("joint", new DiskMaterial(0.5, 0.01, 10, 10, Colors.MATTE_YELLOW), 
				new JointActionType(4.0 /* per time unit, not per time step */, ControlType.CHANGE, ActionType.SPIN));
		
		this.setRoot(40, body);
		this.addNewItem(0, Math.toRadians(-20), 0, 30, "head");
		
		// Legs (0 & 1: Front - 2 & 3: Back) 
		int knees[] = new int[4];
		int joints[] = new int[4];
		
		joints[0] = this.addNewItem(0, Math.toRadians(220), Math.toRadians(-30), 10, "joint");
		for (int j = 0; j < 8; j++) {
			if (j == 3)
				knees[0] = this.addNewItem(-1, Math.toRadians(5-j), Math.toRadians(160), 6, "joint");
			else if (j<=3)
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, "body");
			else
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -1, "body");
		}
		
		joints[1] = this.addNewItem(0, Math.toRadians(210), Math.toRadians(-30), 10, -2, "joint");
		for (int j = 0; j < 8; j++) {
			if (j == 3)
				knees[1] = this.addNewItem(-1, Math.toRadians(5-j), Math.toRadians(160), 6, -2, "joint");
			else if (j<=3)
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -2, "body");
			else
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -3, "body");
		}
		
		joints[2] = this.addNewItem(1, Math.toRadians(-20), Math.toRadians(-138), 10, "joint");
		for (int j = 0; j < 8; j++) {
			if (j == 3)
				knees[2] = this.addNewItem(-1, Math.toRadians(5-j), Math.toRadians(-160), 6, -4, "joint");
			else if (j<=3)
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -4, "body");
			else
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -1, "body");
		}
		
		joints[3] = this.addNewItem(1, Math.toRadians(-10), Math.toRadians(-138), 10, -2, "joint");
		for (int j = 0; j < 8; j++) {
			if (j == 3)
				knees[3] = this.addNewItem(-1, Math.toRadians(5-j), Math.toRadians(-160), 6, -2, "joint");
			else if (j<=3)
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -2, "body");
			else
				this.addNewItem(-1, Math.toRadians(5-j), 0, 6, -3, "body");
		}
		
		// head & neck
		int neck[] = new int[6];
		
		neck[0] = this.addNewItem(0, Math.toRadians(120), Math.toRadians(50), 15, 1, "joint");
		for (int i = 0; i < neck.length-1; i++) {
			neck[i+1] = this.addNewItem(-1, Math.toRadians(10), 0, 15-i/3, 1, "joint");
		}
		this.addNewItem(-1, 0, 0, 23, 1, "body");
				
		
		
		// Front legs movement freedom
		for (int i = 0; i < 2; i++) {
			this.setActionRange(knees[i], ActionType.SPIN, 0, Math.PI*6/7);
			this.setActionRange(joints[i], ActionType.SPIN, -Math.PI/2, Math.PI/5);
		}
		// Back legs movement freedom
		for (int i = 2; i < 4; i++) {
			this.setActionRange(knees[i], ActionType.SPIN, 0, Math.PI*6/7);
			this.setActionRange(joints[i], ActionType.SPIN, -Math.PI*6/7, 0);
		}
		// Neck movement freedom
		this.setActionRange(neck[0], ActionType.SPIN, Math.toRadians(-40), Math.toRadians(80));
		for (int i = 1; i < neck.length; i++) {
			this.setActionRange(neck[i], ActionType.SPIN, Math.toRadians(-20), Math.toRadians(20));
		}
		
	}


}
