package agent;

import ccc.DiskWorldScenario.AgentConstructor;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollisionEventHandler;

/**
 * Agent
 * actuator, size (number of disks), EventHandler, isSticky-feature, body-DiskType (bodyRadius can be set
 * in Constructor)
 */
public class Agent extends AgentConstructor {
	
	private Actuator actuator;
	private int size = 1; // number of disks
	private CollisionEventHandler eventHandler = null;
	private boolean isSticky = false;
	private DiskType body; 

	public Agent(Environment env, Actuator actuator, DiskMaterial bodyMaterial, double bodyRadius) {
		super(env);
		body = new DiskType(bodyMaterial, actuator);
		this.setRoot(bodyRadius, body);
	}
	
	public Agent(Environment env) {
		super(env);
	}

	
	/**
	 * use addNewItem instead of addItem to keep track of size
	 */
	protected int addNewItem(int parentIndex, double positionAngle, double rotationAngle, double radius, String diskType) {
		this.size++;
		return this.addItem(parentIndex, positionAngle, rotationAngle, radius, diskType);
	}
	
	protected int addNewItem(int parentIndex, double positionAngle, double rotationAngle, double radius, DiskType diskType) {
		this.size++;
		return this.addItem(parentIndex, positionAngle, rotationAngle, radius, diskType);
	}
	
	protected int addNewItem(int parentIndex, double positionAngle, double rotationAngle, double radius, int zLevel, String diskType) {
		this.size++;
		return this.addItem(parentIndex, positionAngle, rotationAngle, radius, zLevel, diskType);
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Getter and Setter: Size, Body-Disktype, EventHandler, Actuator                                       */
	///////////////////////////////////////////////////////////////////////////////////////////

	public int getSize() {
		return size;
	}

	public DiskType getBodyDiskType() {
		return body;
	}
	
	public CollisionEventHandler getEventHandler() {
		return eventHandler;
	}

	public void setEventHandler(CollisionEventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}

	public Actuator getActuator() {
		return actuator;
	}

	public void setActuator(Actuator actuator) {
		this.actuator = actuator;
	}
	
	public boolean getIsSticky(){
		return this.isSticky;
	}
	
	public void setIsSticky(boolean isSticky){
		this.isSticky = isSticky;
	}

}
