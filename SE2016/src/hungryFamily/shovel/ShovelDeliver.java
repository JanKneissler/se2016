package hungryFamily.shovel;

import ccc.evaluation.Main;
import ccc.interfaces.AgentState;


public class ShovelDeliver extends ShovelScenarioA {

	private static final boolean HAS_MOUTH = true;

	public ShovelDeliver(){
		super(HAS_MOUTH);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {
		collectFoodInMouth(agentStates, false);
		super.updateAtEndOfTimeStep(agentStates);
	};
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: ShovelDeliver (2,1)";
	}
	@Override
	public String getDescription() {
		return "Agent consits of a body and a shovel as tool. \n"
				+ "The agent gains energy by moving food into the mouth using the shovel. \n"
				+ "Once all food is collected it is resetted.";
	}
	
	
	@Override
	public String getAuthors() {
		return "Die_geballte_kognitive_Intelligenz";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(ShovelDeliver.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}