package hungryFamily.shovel;

import java.util.Random;

import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;

public class ShovelFindAll extends ShovelScenarioB {

	private static final boolean HAS_GLUE = true;
	
	public ShovelFindAll() {
		super(HAS_GLUE);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* ControlledAgentData                                                                   */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates){
		ControlledAgentData[] agentData= super.getControlledAgents(aRandom, eRandom, controlledAgentStates);
	
		// add EventHandler to Agent
		getAgentDisks()[0][0].addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				if(otherDisk instanceof Disk){
					Disk shoveldisk = getAgentDisks()[1][0].getDiskComplex().getDisks().get(1);
					
					// if agent is sticky & touches the gluePoint of the Shovel, set shovel unfixed & merge them
					if(getAgents()[0].getIsSticky() & ((Disk) otherDisk).getDiskType().getMaterial() == gluePoint.getMaterial()){
						agentData[0].getDiskComplexes()[1].setDiskNotFixed(shoveldisk);
						getEnvironment().merge(getAgentDisks()[0][0], (Disk) otherDisk);		
					}
			
				}
			}
		});
		return agentData; 
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: ShovelFindAll (2,3)";
	}
	@Override
	public String getDescription() {
		return "The agents acquires glue feature by traversing a glue area. \n"
				+ "Therewith he can merge with the shovel \n"
				+ "and use it to deliver food into the mouth. \n"
				+ "the agent gets harmed by touching food with bodyparts other than the shovel.";
	}
	@Override
	public String getAuthors() {
		return "Max, Benny";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(ShovelFindAll.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}


}

