package hungryFamily.shovel;

import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.interfaces.CognitiveController;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import hungryFamily.HungryFamilyEnvironment;
import utils.Colors;

/**
 * this class is a scenario inheriting from HungryFamilyEnvironment, with ShovelAgent
 */
public class ShovelScenarioA extends HungryFamilyEnvironment {

	private static final int NUMBER_OF_AGENTS = 1;
	private static final int NUMBER_OF_FOODDISKS = 3;
	private static final int NUMBER_OF_POISONDISKS = 3;

	private static final boolean HAS_GLUE = false;
	private static final boolean BODY_MUSTNOT_TOUCH_FOOD = true;

	private static boolean movementConsumesEnergy = true;

	public ShovelScenarioA(boolean hasMouth){
		super(NUMBER_OF_AGENTS, NUMBER_OF_FOODDISKS, NUMBER_OF_POISONDISKS, hasMouth, HAS_GLUE, BODY_MUSTNOT_TOUCH_FOOD);
	}


	/** create Agents */
	@Override
	protected void createAgents() {
		double agentBodyRadius = ScenarioUtils.uniform(new Random(), 20, 40);

		double maxBackwardDistance = 10; // maximum distance translated backwards in one time step
		double maxForwardDistance = 10; // maximum distance translated forward in one time step
		double maxRotationValue = 10;// the maximum angular speed/rotation angle by which the disk can rotate (in radian per time unit or radian), (non-negative)
		double rotationEnergyConsumption = 1*Math.pow(10, -11); // energy consumption for full 360 rotation per mass momentum unit, (non-negative)
		double moveEnergyConsumption = 0; // energy consumption for motion by 1 distance unit and per mass unit, (non-negative)

		Actuator actuator;
		if (movementConsumesEnergy) {
			actuator = new Mover(maxBackwardDistance, maxForwardDistance, maxRotationValue, rotationEnergyConsumption, moveEnergyConsumption);
		} else
			actuator = new Mover(maxBackwardDistance, maxForwardDistance, maxRotationValue, 0, moveEnergyConsumption);

		Shovel shoveller = new Shovel(getEnvironment(), actuator, new Colors(1, 0.01, 1, 1, 0), agentBodyRadius, envRandom);
		this.setAgents(new Agent[] {shoveller}); 
	}



	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Shovel (2,0) and (2,1)";
	}
	@Override
	public String getDescription() {
		return "The agent consits of a body and a shovel as tool. \n"
				+ "Using the shovel he can move food.";
	}
	@Override
	public String getAuthors() {
		return "Die_geballte_kognitive_Intelligenz";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {		
			always(0, 0.0);
			always(1, 0.0);

			//onKey(char keyChar, int actuatorIndex, double value, boolean increment);
			onKey('S', 0, -1, true); // decrement first value in actuator array (index 0) by 1.3
			onKey('W', 0, +1, true);
			onKey('A', 1, .02, false);
			onKey('D', 1, -.02, false);

			setLogMessage("Use the following keys to control the agent:\n" +
					"a -> turn left \n" +
					"d -> turn right \n" +
					"w -> move forward \n" +
					"s -> move backwards \n");
		}
	}



	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}



}