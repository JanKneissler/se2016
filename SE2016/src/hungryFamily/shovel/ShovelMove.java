package hungryFamily.shovel;

import java.util.Random;

import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import environmentalObjects.Food;

public class ShovelMove extends ShovelScenarioA {

	private static final boolean HAS_MOUTH = false;

	public ShovelMove(){
		super(HAS_MOUTH);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* ControlledAgentData                                                                   */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates){
		ControlledAgentData[] agentData = super.getControlledAgents(aRandom, eRandom, controlledAgentStates);
		
		// agent gains energy for pushing foodDisks with shovel
		// EventHandler only for ShovelDisks
		for (int i = 1; i < getAgentDisks()[0].length; i++) {
			getAgentDisks()[0][i].addEventHandler(new CollisionEventHandler() {
				@Override
				public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
					if (otherDisk instanceof Disk && ((Disk) otherDisk).getDiskType().equals(Food.foodType)) {
						controlledAgentStates[0].incEnergyGained(energyGainPerItem /100);
					}
				}
			});
		}
		return agentData; 
	}



	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: ShovelMove (2,0)";
	}
	@Override
	public String getDescription() {
		return "The agent consists of a body and a shovel as tool. \n"
				+ "The agent gains energy by moving food using the shovel.";
	}
	@Override
	public String getAuthors() {
		return "Die_geballte_kognitive_Intelligenz";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(ShovelMove.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
