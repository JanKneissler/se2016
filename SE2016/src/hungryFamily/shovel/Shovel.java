package hungryFamily.shovel;

import java.util.Random;
import agent.Agent;
import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.Environment;
import diskworld.interfaces.Actuator;
import utils.Colors;


/**Shovel-Agent
 * consists of a main body and 9 disks forming a shovel
 */
public class Shovel extends Agent {

	public Shovel(Environment env, Actuator actuator, Colors bodyMaterial, double bodyRadius, Random envRandom) {
		super(env, actuator, bodyMaterial, bodyRadius);
		double radiusArm =ScenarioUtils.uniform(envRandom, 3, 5);
		
		// Actuator == null => Shovel is used as a tool, which can be picked up
		if(actuator == null){
			this.createDiskType("arm", new Colors(1, 0.01, 1, 0.01, -0.8));
			
			this.addItem(0, Math.toRadians(90), 0, radiusArm, "arm");
			this.addItem(0, Math.toRadians(-90), 0, radiusArm, "arm");
			
			
			this.addNewItem(0, 0, 0, radiusArm, "arm");
			
			for (int i = 0; i < 2; i++) {			
				this.addNewItem(-1, 0, 0, radiusArm, "arm");
			}
			
			this.addNewItem(-1, Math.toRadians(90), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(-15), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(-15), 0, radiusArm, "arm");

			this.addNewItem( -4, Math.toRadians(-90), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(15), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(15), 0, radiusArm, "arm");
		}
		// Actuator != null => Shovel can get moved, used in ShovelSzenario
		else{
			this.createDiskType("arm", new Colors(1, 0.01, 1, 0.01, -0.8));
			
			
			for (int i = 0; i < 3; i++) {			
				this.addNewItem(-1, 0, 0, radiusArm, "arm");
			}
			
			this.addNewItem(-1, Math.toRadians(90), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(-15), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(-15), 0, radiusArm, "arm");

			this.addNewItem( 3, Math.toRadians(-90), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(15), 0, radiusArm, "arm");
			this.addNewItem(-1, Math.toRadians(15), 0, radiusArm, "arm");
		}
		
		
	}
	
	


}
