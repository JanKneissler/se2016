package hungryFamily.shovel;

import java.util.Random;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;


public class ShovelFindTool extends ShovelScenarioB {

private static final boolean HAS_GLUE = false;
	
	public ShovelFindTool() {
		super(HAS_GLUE);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* ControlledAgentData                                                                   */
	///////////////////////////////////////////////////////////////////////////////////////////

	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates){
		ControlledAgentData[] agentData= super.getControlledAgents(aRandom, eRandom, controlledAgentStates);
		
		// add EventHandler to Agent
		getAgentDisks()[0][0].addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				if(otherDisk instanceof Disk){
					Disk shoveldisk = getAgentDisks()[1][0].getDiskComplex().getDisks().get(1);
					
					// if agent is sticky & touches the gluePoint of the Shovel, set shovel unfixed & merge them
					if(((Disk) otherDisk).getDiskType().getMaterial() == gluePoint.getMaterial()){
						agentData[0].getDiskComplexes()[1].setDiskNotFixed(shoveldisk);
						getEnvironment().merge(getAgentDisks()[0][0], (Disk) otherDisk);		
					}
			
				}
			}
		});
		return agentData; 
	}
	


	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: ShovelFindTool (2,2)";
	}
	@Override
	public String getDescription() {
		return "The agent has to find the shovel and merge \n"
				+ "to deliver food into the mouth with it. \n"
				+ "he gets harmed by touching food with bodyparts other than the shovel.";
	}
	@Override
	public String getAuthors() {
		return "Max, Benny";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(ShovelFindTool.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}


}

