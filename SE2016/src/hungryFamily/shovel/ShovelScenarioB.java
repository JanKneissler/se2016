package hungryFamily.shovel;

import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskType;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import hungryFamily.HungryFamilyEnvironment;
import utils.Colors;

public class ShovelScenarioB extends HungryFamilyEnvironment {

	protected DiskType arm, gluePoint;

	private static final int NUMBER_OF_AGENTS = 1;
	protected static final int NUMBER_OF_FOODDISKS = 3;
	private static final int NUMBER_OF_POISONDISKS = 0;

	private static final boolean HAS_MOUTH = true;
	private static final boolean BODY_MUSTNOT_TOUCH_FOOD = true;

	private static boolean movementConsumesEnergy = true;

	public ShovelScenarioB(boolean hasGlue) {
		super(NUMBER_OF_AGENTS, NUMBER_OF_FOODDISKS, NUMBER_OF_POISONDISKS, HAS_MOUTH, hasGlue, BODY_MUSTNOT_TOUCH_FOOD);
	}

	/** create Agents */
	@Override
	protected void createAgents() {
		double agentBodyRadius = ScenarioUtils.uniform(new Random(), 20, 40);

		double rotationEnergyConsumption = 1*Math.pow(10, -11);
		Actuator actuator;
		if (movementConsumesEnergy) {
			actuator = new Mover(10, 10, 1, rotationEnergyConsumption, 0);
		} else
			actuator = new Mover(10, 10, 1, 0, 0);

		Agent agent = new Agent(getEnvironment(), actuator, new Colors(1, 0.01, 1, 1, 0), agentBodyRadius);
		
		arm = new DiskType(new Colors(1, 0.01, 1, 1, 0));
		gluePoint = new DiskType(new Colors(1, 0.01, 1, 1, -1.0));
		Shovel shovel = new Shovel(getEnvironment(), null, (Colors)gluePoint.getMaterial(), 3, envRandom);
		
		this.setAgents(new Agent[] {agent, shovel});		
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* ControlledAgentData                                                                   */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates){
		agentData= super.getControlledAgents(aRandom, eRandom, controlledAgentStates);
		
		// set the shovel fixed
		Disk shoveldisk = getAgentDisks()[1][0].getDiskComplex().getDisks().get(1);
		agentData[0].getDiskComplexes()[1].setDiskFixed(shoveldisk);
		return agentData; 
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {
		collectFoodInMouth(agentStates, false);
		super.updateAtEndOfTimeStep(agentStates);
	};

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Shovel (2,2) and (2,3)";
	}
	@Override
	public String getDescription() {
		return "The agent has to merge with shovel to deliver food.";
	}
	@Override
	public String getAuthors() {
		return "Max, Benny";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			always(0, 0.0);
			always(1, 0.0);
			onKey('S', 0, -0.5, true);
			onKey('W', 0, +0.5, true);
			onKey('A', 1, .1, true);
			onKey('D', 1, -.1, true);

			setLogMessage("Use the following keys to control the agent:\n" +
					"a,d -> left/right\n" +
					"w,s -> up/down\n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}


}

