package hungryFamily;

import java.awt.Color;
import java.util.Iterator;
import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.Environment;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import environmentalObjects.Area;
import environmentalObjects.Food;
import environmentalObjects.Poison;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;
import utils.Colors;

/**
 * This class provides the environment for all scenarios of the hungryFamily.
 * The environment can contain a number of food and poison items
 * as well as a mouth and/or glueArea.
 */
public abstract class HungryFamilyEnvironment extends GlobalInformationFixedDiskNumScenario {

	private static final double FLOOR_TILE_SIZE = 100;			// 10 x 10 floor tiles

	private  boolean bodyMustNotTouchFood = false;

	// energy and harm
	protected double energyGainPerItem = 0.02;
	protected double energyGainOnWin = 0.05;
	private double harmInflictedPerPoisonContact = 0.01;
	private double initialEnergyLevel = 0.2;
	private int collectedFood = 0;

	// agent
	private Agent[] agents;
	protected Disk[][] agentDisks;
	private DiskComplex[] agentComplexes;						// 2D: agentDisks for each agent
	protected ControlledAgentData agentData[];

	// food and poison
	protected Food[] food;
	protected Poison[] poison;

	// mouth and glue
	private boolean hasMouth;
	private boolean hasGlueArea;
	private Area mouth;
	private Area glue;
	protected Point mouthPos;
	protected Point gluePos;

	private boolean areaDisksOn = true;							// whether mouth and glueArea are visible as disks
	private boolean areaFloorOn = true;							// whether mouth and glueArea are visible as floorType

	// all Disks
	private Disk[][] allDisks;									// Food, Poison, Mouth and Glue: DiskComplex x Disk
	private Disk[] disks;

	protected Random envRandom = new Random();

	// colors for mouth and glueArea
	private final double MOUTH_COLOR_INDEX = 0.5;
	protected final double GLUE_COLOR_INDEX = -0.2;

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Constructor                                                                           */
	///////////////////////////////////////////////////////////////////////////////////////////

	public HungryFamilyEnvironment(int numberOfAgents, int numberOfFood, int numberOfPoison, boolean hasMouth, boolean hasGlue) {
		super(false, true, FLOOR_TILE_SIZE);
		this.agents = new Agent[numberOfAgents];
		this.food = new Food[numberOfFood];
		this.poison = new Poison[numberOfPoison];
		this.hasMouth = hasMouth;
		this.hasGlueArea = hasGlue;
	}

	public HungryFamilyEnvironment(int numberOfAgents, int numberOfFooddisks, int numberOfPoisondisks,
			boolean hasMouth, boolean hasGlue, boolean bodyMustnotTouchFood) {
		this(numberOfAgents, numberOfFooddisks, numberOfPoisondisks, hasMouth, hasGlue); 
		this.bodyMustNotTouchFood = bodyMustnotTouchFood; 
	}
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* initialize environment, create environmental objects                                  */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {

		// ----------------------------------------------------------------
		// positions for mouth and glueArea
		generateMouthAndGluePositions();

		// ----------------------------------------------------------------
		// paint mouth and glueArea on floor (if wanted)
		paintFloor();

		// ----------------------------------------------------------------
		// create Disks for mouth and glueArea (if wanted)
		if (areaDisksOn) {
			if (hasMouth) {
				mouth = new Area(environment, random, MOUTH_COLOR_INDEX);
				double mouthDiskX = (mouthPos.getX() * 100) + Area.getRadius()+1;
				double mouthDiskY = (mouthPos.getY() * 100) + Area.getRadius()+1;
				DiskComplex m = mouth.createDiskComplex(mouthDiskX, mouthDiskY, 0, 1);
				m.getDisks().get(0).setZLevel(-10);
			}
			if (hasGlueArea) {
				glue = new Area(environment, random, GLUE_COLOR_INDEX);
				double glueDiskX = (gluePos.getX() * 100) + Area.getRadius()+1;
				double glueDiskY = (gluePos.getY() * 100) + Area.getRadius()+1;
				DiskComplex g = glue.createDiskComplex(glueDiskX, glueDiskY, 0, 1);
				g.getDisks().get(0).setZLevel(-10);
			}
		}

		// ----------------------------------------------------------------
		// Objects: Food + Poison

		// Food
		for (int i = 0; i < food.length; i++) {
			food[i] = new Food(environment, random, ScenarioUtils.uniform(random, 7, 17));
			boolean wasPlaced = false;
			do {
				wasPlaced = food[i].createObject();
			} while(!wasPlaced);
		}
		// Poison + EventHandler
		for (int i = 0; i < poison.length; i++) {
			poison[i] = new Poison(environment, random, ScenarioUtils.uniform(random, 18, 28));
			boolean wasPlaced = false;
			do {
				wasPlaced = poison[i].createObject();
			} while(!wasPlaced);
		}


		// ----------------------------------------------------------------
		// all Disks [DiskComplexes][Disks]

		// initialize allDisks[][]
		if (areaDisksOn) {
			if (hasMouth && hasGlueArea) {
				allDisks = new Disk[food.length + poison.length + 2][];
			} else if (hasMouth || hasGlueArea) {
				allDisks = new Disk[food.length + poison.length + 1][];
			} else {
				allDisks = new Disk[food.length + poison.length][];
			}
		} else {
			allDisks = new Disk[food.length + poison.length][];
		}

		Iterator<DiskComplex> it = environment.getDiskComplexes().iterator();
		DiskComplex dc = null;

		// fill allDisks[][]
		for (int i = 0; i < allDisks.length; i++) {
			dc = it.next();
			allDisks[i] = new Disk[dc.getDisks().size()];
			for (int j = 0; j < allDisks[i].length; j++) {
				allDisks[i][j] = dc.getDisks().get(j);
			}
		}


		// ----------------------------------------------------------------
		// add EventHandler to Poison-Disks, so Agent gets harm when touching them
		for (int i = 0; i < allDisks.length; i++) {
			for (int j = 0; j < allDisks[i].length; j++) {
				if (allDisks[i][j].getDiskType().equals(Poison.poisonType)) {
					allDisks[i][j].addEventHandler(new CollisionEventHandler() {

						@Override
						public void collision(final CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
							if (otherDisk instanceof Disk && !((Disk) otherDisk).getDiskType().equals(Poison.poisonType)) {
								getControlledAgentStates()[0].incHarmInflicted(harmInflictedPerPoisonContact);
							}
						}
					});
				}
				// harm for scenarios in which agent must not touch the fooddisks without the tool
				if (allDisks[i][j].getDiskType().equals(Food.foodType) && bodyMustNotTouchFood) {
					allDisks[i][j].addEventHandler(new CollisionEventHandler() {

						@Override
						public void collision(final CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
							if (otherDisk instanceof Disk && ((Disk) otherDisk).getDiskType().equals(((Agent) getAgents()[0]).getBodyDiskType())) {
								getControlledAgentStates()[0].incHarmInflicted(harmInflictedPerPoisonContact);
							}
						}
					});
				}
			}
		}


		// ----------------------------------------------------------------
		// Set shiftResistance for joints and gravity model
		environment.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.0);
		environment.getPhysicsParameters().setGravityModel(new ConstantGravityModel(0.0));

	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* create agents                                                                         */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {

		// calls method from inheriting scenario class that defines all agents in agent-array
		createAgents();

		// agentDisks for each agent
		agentDisks = new Disk[agents.length][];
		for (int i = 0; i < agents.length; i++) {
			agentDisks[i] = new Disk[agents[i].getSize()];
		}

		agentData = new ControlledAgentData[agents.length];
		agentComplexes = new DiskComplex[agents.length];

		// create and place all agents and their disks
		for (int i = 0; i < agents.length; i++) {
			double scaleFactor = 1.0;
			controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
			do {
				double posx = ScenarioUtils.uniform(eRandom, 90.0, getEnvironment().getMaxX());
				double posy = ScenarioUtils.uniform(eRandom, 0.0, getEnvironment().getMaxY());
				double orientation = 0.0;
				agentComplexes[i] = agents[i].createDiskComplex(posx, posy, orientation, scaleFactor);
			} while (agentComplexes[i] == null);

		}

		// create all agents
		agentData = new ControlledAgentData[] 
				{AgentConstructor.createControlledAgentWithMultipleDiskComplexes(agentComplexes)};

		for (int i = 0; i < agents.length; i++) {
			for (int j = 0; j < agents[i].getSize(); j++) {				
				agentDisks[i][j] = agentData[0].getDiskComplexes()[i].getDisks().get(j);
				// only if an EventHandler is defined add it to this disk
				if (agents[i].getEventHandler()!=null)
					agentDisks[i][j].addEventHandler(agents[i].getEventHandler());
			}
		}

		// set all disks
		this.setDisks();

		return agentData;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* agent: additional methods                                                             */
	///////////////////////////////////////////////////////////////////////////////////////////

	public void setAgents(Agent[] agents){
		this.agents = agents;
	}

	public Agent[] getAgents(){
		return this.agents;
	}

	@Override
	public int determineNumberOfControlledAgents() {
		return 1; // before: agents.length;
	}

	/** implemented by inheriting scenario class */
	protected abstract void createAgents();


	public Disk[][] getAgentDisks(){
		return agentDisks;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* update at end of time step                                                            */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {

		// if all food is eaten: reset
		if (collectedFood == food.length) {
			agentStates[0].incEnergyGained(energyGainOnWin);
			resetFoodDisks();		// reset Food
			nextEpisode();
			collectedFood = 0;
		}


		// if agent is in glueArea -> set "isSticky = true" and change color to GlueColor
		if (hasGlueArea) {
			Disk body = agentDisks[0][0];
			double posX = Math.floor(body.getX() / 100);
			double posY = Math.floor(body.getY() / 100);
			if (posX == gluePos.getX() && posY == gluePos.getY()) {
				if (!agents[0].getIsSticky()) {
					agentStates[0].incEnergyGained(energyGainPerItem);
					agents[0].setIsSticky(true);
					agentDisks[0][0].getDiskType().getMaterial().setDisplayColor(Colors.getDisplayColorByIndex(GLUE_COLOR_INDEX));
					nextEpisode();
				}
			}
		}
	}



	///////////////////////////////////////////////////////////////////////////////////////////
	/* pool of methods that can be used in updateAtEndOfTimeStep                             */
	///////////////////////////////////////////////////////////////////////////////////////////

	protected void collectFoodInMouth(AgentState[] agentStates, boolean GlueScenario) {
		// food located in mouth -> setFixed and count collected Food
		if (hasMouth) {
			for (int i = 0; i < allDisks.length; i++) {
				for (int j = 0; j < allDisks[i].length; j++) {
					Disk d = allDisks[i][j];
					// check if it's a foodDisk
					if (d.getDiskType().equals(Food.foodType) && !d.isMarkedFixed()) {

						double posX = Math.floor(d.getX() / 100);
						double posY = Math.floor(d.getY() / 100);

						if (posX == mouthPos.getX() && posY == mouthPos.getY()) {
							System.out.println("innen: " + d.getX() + " | " + d.getY());

							// only for non-complex but sticky agent as in GlueScenario
							if (GlueScenario) {
								agentDisks[0][0].getDiskComplex().splitFromNeighbors(agentDisks[0][0]);
							}

							shiftIntoMouth(d);
							++collectedFood;
							agentStates[0].incEnergyGained(energyGainPerItem);
						}}}}}
	}


	// shift a disk into center of MouthArea, in different Z-Level
	protected void shiftIntoMouth(Disk foodDisk) {
		double mouthX = (mouthPos.getX() * 100) + Area.getRadius();
		double mouthY = (mouthPos.getY() * 100) + Area.getRadius();
		double foodX = foodDisk.getX();
		double foodY = foodDisk.getY();
		double shiftx = mouthX - foodX;
		double shifty = mouthY - foodY;

		foodDisk.setZLevel(20);
		foodDisk.getDiskComplex().teleport(shiftx, shifty, 0.0);
		foodDisk.getDiskComplex().setDiskFixed(foodDisk);
	}

	// ----------------------------------------------------------------
	// reset-methods
	protected void resetFoodDisks() {

		for (int i = 0; i < allDisks.length; i++) {
			for (int j = 0; j < allDisks[i].length; j++) {
				Disk d = allDisks[i][j];
				// reset Food
				if (d.getDiskType().equals(Food.foodType)) {
					d.getDiskComplex().setDiskNotFixed(d);
					d.setZLevel(0);
					double newX, newY;
					do {
						newX = ScenarioUtils.uniform(new Random(), 0, getEnvironment().getMaxX());
						newY = ScenarioUtils.uniform(new Random(), 0, getEnvironment().getMaxY());
					} while(!getEnvironment().canTeleport(d, newX, newY, 0));
				}
				d.getDiskComplex().setVelocity(0, 0);
			}
		}
	}

	protected void resetPosition(Disk d) {
		double posx,posy;
		do {
			posx = ScenarioUtils.uniform(new Random(), 0.0, getEnvironment().getMaxX());
			posy = ScenarioUtils.uniform(new Random(), 0.0, getEnvironment().getMaxY());			
		} while(!getEnvironment().canTeleport(d, posx, posy, 0));		
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* set disks: should only be invoked once, when the scenario is created                  */
	///////////////////////////////////////////////////////////////////////////////////////////

	private void setDisks(){
		// initialize disks[]
		int sum = 0;
		for (int i = 0; i < agentDisks.length; i++) {
			sum += agentDisks[i].length;
		}
		for (int i = 0; i < allDisks.length; i++) {
			sum += allDisks[i].length;
		}
		disks = new Disk[sum];

		int n = 0;
		// agentDisks
		for (int i = 0; i < agentDisks.length; i++) {
			for (int j = 0; j < agentDisks[i].length; j++) {
				disks[n] = agentDisks[i][j];
				n++;
			}
		}
		// allDisks
		for (int i = 0; i < allDisks.length; i++) {
			for (int j = 0; j < allDisks[i].length; j++) {
				disks[n] = allDisks[i][j];
				n++;
			}
		}
		this.setDisks(disks);
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* additional methods                                                                    */
	///////////////////////////////////////////////////////////////////////////////////////////


	// ----------------------------------------------------------------
	// Methods used in initialize

	private void generateMouthAndGluePositions() {
		Point c;
		if (hasMouth) {
			int mouthx = ScenarioUtils.uniform(envRandom, 0, getEnvironment().getFloor().getNumX()-1);
			int mouthy = ScenarioUtils.uniform(envRandom, 0, getEnvironment().getFloor().getNumY()-1);
			c = new Point(mouthx, mouthy);
			mouthPos = c;
		}
		if (hasGlueArea) {
			boolean isUnique = true;
			do {
				int gluex = ScenarioUtils.uniform(envRandom, 0, getEnvironment().getFloor().getNumX()-1);
				int gluey = ScenarioUtils.uniform(envRandom, 0, getEnvironment().getFloor().getNumY()-1);
				c = new Point(gluex, gluey);
				if (mouthPos != null && gluex == mouthPos.getX() && gluey == mouthPos.getY()) {
					isUnique = false;
				}
			} while(!isUnique);
			gluePos = c;
		}
	}


	private void paintFloor() {
		Floor floor = getEnvironment().getFloor();
		FloorCellType stone = new FloorCellType(ScenarioUtils.uniform(envRandom, 0d, 1d), Color.DARK_GRAY);
		System.out.println(stone.getFrictionCoefficient());
		floor.fill(stone);

		if (areaFloorOn) {
			if (hasMouth) {
				FloorCellType grass = new FloorCellType(FloorCellType.GRASS.getFrictionCoefficient(), Colors.getDisplayColorByIndex(MOUTH_COLOR_INDEX));
				floor.setType( (int)mouthPos.getX(), (int)mouthPos.getY(), grass);
			}
			if (hasGlueArea) {
				FloorCellType water = new FloorCellType(FloorCellType.GRASS.getFrictionCoefficient(), Colors.getDisplayColorByIndex(GLUE_COLOR_INDEX));
				floor.setType((int)gluePos.getX(), (int)gluePos.getY(), water);	
			}
		}
	}

	// ----------------------------------------------------------------
	@Override
	public double getScore() {
		return 0;
	}


}

