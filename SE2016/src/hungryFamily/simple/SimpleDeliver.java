package hungryFamily.simple;

import ccc.evaluation.Main;
import ccc.interfaces.AgentState;

public class SimpleDeliver extends SimpleScenario {

	private static final boolean HAS_MOUTH = true;

	public SimpleDeliver() {
		super(HAS_MOUTH);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {
		
		collectFoodInMouth(agentStates, true);
		super.updateAtEndOfTimeStep(agentStates);
	};

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: SimpleDeliver (0,1)";
	}

	@Override
	public String getDescription() {
		return "Simple scenarios without tools or other agent features. \n"
				+ "Agent gains energy by pushing food into mouth. \n"
				+ "and gets harmed at contact with poison.";
	}

	@Override
	public String getAuthors() {
		return "Flavia";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(SimpleDeliver.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}
}
