package hungryFamily.simple;

import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import environmentalObjects.Food;

public class SimpleMove extends SimpleScenario {

	private static final boolean HAS_MOUTH = false;

	public SimpleMove() {
		super(HAS_MOUTH);
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {

		// agent gains energy for pushing foodDisks
		getAgentDisks()[0][0].addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				if (otherDisk instanceof Disk && ((Disk) otherDisk).getDiskType().equals(Food.foodType)) {
					agentStates[0].incEnergyGained(energyGainPerItem / 1000);
				}
			}
		});

		super.updateAtEndOfTimeStep(agentStates);
	};


	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: SimpleMove (0,0)";
	}

	@Override
	public String getDescription() {
		return "Simple scenarios without tools or other agent features. \n"
				+ "Agent gains energy by pushing food around \n"
				+ "and gets harmed at contact with poison.";
	}

	@Override
	public String getAuthors() {
		return "Flavia";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(SimpleMove.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}