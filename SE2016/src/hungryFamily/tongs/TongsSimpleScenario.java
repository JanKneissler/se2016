/**
 * TODO: 
 * 
 * Ggf. Create Agents, sowie initialisierte Werte ausmisten
 */

package hungryFamily.tongs;

import java.util.ArrayList;
import java.util.List;

import agent.Agent;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskMaterial;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import hungryFamily.HungryFamilyEnvironment;
import utils.Colors;
import diskworld.Environment;

public class TongsSimpleScenario extends HungryFamilyEnvironment {
	
	private static int numberOfAgentDisks = 20; 
	private static final int NUMBER_OF_AGENTS = 1;
	private static final int NUMBER_OF_FOODDISKS = 3;
	private static final int NUMBER_OF_POISONDISKS = 3;
	private static final double ENERGYGAIN_PER_ITEM = 0.01;
	private static final double ENERGYGAIN_ON_WIN = 0.3;
	private static final boolean HAS_GLUE = false;
	
	private static final boolean BODY_MUSTNOT_TOUCH_FOOD = true;
	private boolean hasMouth;
	
	private boolean releaseFood;
	private Disk capturedDisk;
	
	private List <Disk> deliveredFoodDisks ;
	
	private static boolean movementConsumesEnergy = true;
	
	public TongsSimpleScenario(boolean hasMouth){
		super(NUMBER_OF_AGENTS, NUMBER_OF_FOODDISKS, NUMBER_OF_POISONDISKS, hasMouth, HAS_GLUE, BODY_MUSTNOT_TOUCH_FOOD);	this.hasMouth = hasMouth;
	}
	
	/**
	 * create Agents 
	 * (create more than one for tool that is not attached firmly to agent)
	 */
	@Override
	protected void createAgents() {
		 deliveredFoodDisks = new ArrayList <Disk> ();

		releaseFood = false;
		capturedDisk = null;
		// sensors and actuators
		double maxBackwardDistance = 2; // maximum distance translated backwards in one time step
		double maxForwardDistance = 2; // maximum distance translated forward in one time step
		double maxRotationValue = 2; // the maximum angular speed/rotation angle by which the disk can rotate (in radian per time unit or radian), (non-negative)
		double rotationEnergyConsumption = 0; // energy consumption for full 360 rotation per mass momentum unit, (non-negative)
		double moveEnergyConsumption = 0; // energy consumption for motion by 1 distance unit and per mass unit, (non-negative)

		Actuator actuator;
		if (movementConsumesEnergy) {
			actuator = new Mover(maxBackwardDistance, maxForwardDistance, maxRotationValue, rotationEnergyConsumption, moveEnergyConsumption);
		} else
			actuator = new Mover(maxBackwardDistance, maxForwardDistance, maxRotationValue, 0, moveEnergyConsumption);

		Colors rootType = new Colors(1, 0.01, 1, 1, 0);
		Environment env = getEnvironment();
		env.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.0);


		Tongs tongsMover = new Tongs(env, actuator, rootType, 30, envRandom);

		this.setAgents(new Agent[] {tongsMover});
	}
	
	public boolean captured(){
	int sizeOfAgentComplex = getAgentDisks()[0][0].getDiskComplex().getDisks().size();
	return (sizeOfAgentComplex > numberOfAgentDisks);
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                   				                                 */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		
		if(captured()){
			
			DiskMaterial foodMaterial = food[0].getDiskType().getMaterial();
			for (int i = 0; i < getAgentDisks()[0][0].getDiskComplex().getDisks().size(); i++) {
				if(getAgentDisks()[0][0].getDiskComplex().getDisks().get(i).getDiskType().getMaterial() == foodMaterial){
					capturedDisk = getAgentDisks()[0][0].getDiskComplex().getDisks().get(i);
				}
			}
			
			double capturedX = Math.floor(capturedDisk.getX() / 100);
			double capturedY = Math.floor(capturedDisk.getY() / 100);
			
			// set foodDisk fixed when brought over to mouth-position
			if(hasMouth && capturedX == mouthPos.getX() && capturedY == mouthPos.getY()){
				releaseFood = true;
				capturedDisk.applyImpulse(-100 * capturedDisk.getMass() * capturedDisk.getOrientation() , -100 * capturedDisk.getMass() * capturedDisk.getOrientation());
				capturedDisk.getDiskComplex().setDiskFixed(capturedDisk);
			}
			else if(!hasMouth){
				releaseFood = true;
			}
			else releaseFood = false;

			
		}
		// if foodDisk was released over mouth, agent gains Energy (in TongsDeliver)
		if(hasMouth && !captured() && releaseFood && Math.floor(capturedDisk.getX() / 100) == mouthPos.getX() && Math.floor(capturedDisk.getY() / 100) == mouthPos.getY()){
			agentState[0].incEnergyGained(ENERGYGAIN_PER_ITEM);
			shiftIntoMouth(capturedDisk);
			if(!deliveredFoodDisks.contains(capturedDisk)){
				deliveredFoodDisks.add(capturedDisk);
			}
			capturedDisk = null;
			releaseFood = false;

		}
		// if foodDisk is released, agent gains Energy (in TongsMove)
		else if(!hasMouth && !captured() && releaseFood){
			agentState[0].incEnergyGained(ENERGYGAIN_PER_ITEM);
			if(!deliveredFoodDisks.contains(capturedDisk)){
				deliveredFoodDisks.add(capturedDisk);
			}
			capturedDisk = null;
			releaseFood = false;
		}

		// if all foodDisks were delivered, reset Positions, agent gains Energy. then -> nextEpisode
		if(deliveredFoodDisks !=null && deliveredFoodDisks.size() == NUMBER_OF_FOODDISKS){
			for (int i = 0; i < NUMBER_OF_FOODDISKS; i++) {
				resetPosition(deliveredFoodDisks.get(i));
				deliveredFoodDisks.get(i).getDiskComplex().setDiskNotFixed(deliveredFoodDisks.get(i));
			}
			agentState[0].incEnergyGained(ENERGYGAIN_ON_WIN);
			deliveredFoodDisks = new ArrayList<Disk>();
			nextEpisode();
		}

	}

	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Scenario (1, 3)";
	}
	@Override
	public String getDescription() {
		return "Instance of HungryFamily: most complex agent, handeling tongs. Simple events (1,3)";
	}
	@Override
	public String getAuthors() {
		return "Max";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {


		@Override
		public void addKeyEvents() {

			//onKey(char keyChar, int actuatorIndex, double value, boolean increment);
			//onKey(char keyChar, int actuatorIndex, double value, boolean increment);
			always(0, 0.0);
			always(1, 0.0);
			always(2, 0.0);
			always(3, 0.0);

			onKey('L', 0, -1, false);
			onKey('K', 0, 1, false);
			onKey('P', 1, -1, false); // decrement first value in actuator array (index 0) by 1.3
			onKey('O', 1, 1, false);
			onKey('S', 2, -1, false); // decrement first value in actuator array (index 0) by 1.3
			onKey('W', 2, +1, false);
			onKey('A', 3, .02, false);
			onKey('D', 3, -.02, false);

			setLogMessage("Use the following keys to control the agent:\n" +
					"a -> turn left \n" +
					"d -> turn right \n" +
					"w -> move forward \n" +
					"s -> move backward \n" +
					"k -> close right finger \n" +
					"l -> open right finger \n" +
					"o -> open left finger \n" +
					"p -> close left finger");
		}
	}


	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}
}
