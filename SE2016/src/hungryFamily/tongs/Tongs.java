package hungryFamily.tongs;

import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.ScenarioUtils;
import diskworld.Environment;
import diskworld.actions.JointActionType;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actions.JointActionType.ControlType;
import diskworld.interfaces.Actuator;
import utils.Colors;

public class Tongs extends Agent {
	public int elbowId, elbowId2;
	public int mergeDisk1, mergeDisk2;
	

	public Tongs(Environment env, Actuator actuator, Colors bodyMaterial, double bodyRadius, Random random) {
		super(env, actuator, bodyMaterial, bodyRadius);
		
		int bigRadius = ScenarioUtils.uniform(random, 5, 6); 
		int smallRadius = ScenarioUtils.uniform(random, 2, 4); 
		
		this.createDiskType("limbType", new Colors(1, 0.01, 1, 1, -0.8));
		// Gelenk - Typ
		this.createJointType("elbow", new Colors(1, 0.01, 1, 1, -1d), new JointActionType(0.5, ControlType.CHANGE, ActionType.SPIN));
		// Agentenrumpf
		this.addNewItem(-1, Math.toRadians(0), 0, bigRadius, "limbType");
		// linke Zange
		elbowId = this.addNewItem(-1, Math.toRadians(270), 0, bigRadius, "elbow");
		
		this.addNewItem(-1, Math.toRadians(90), 0, smallRadius, "limbType");

		for (int i = 0; i < 6; i++) {
			this.addNewItem(-1, Math.toRadians(0), 0, smallRadius, "limbType");
		}
		mergeDisk1 = this.addNewItem(-1, Math.toRadians(0), 0, smallRadius, "limbType");

		// rechte Zange
		elbowId2 = this.addNewItem(elbowId - 1, Math.toRadians(90), 0, bigRadius, "elbow");
		this.addNewItem(-1, Math.toRadians(270), 0, smallRadius, "limbType");

		for (int i = 0; i < 6; i++) {
			this.addNewItem(-1, Math.toRadians(0), 0, smallRadius, "limbType");
		}
		mergeDisk2 =this.addNewItem(-1, Math.toRadians(0), 0, smallRadius, "limbType");
	}

	
}
