package hungryFamily.tongs;

import ccc.evaluation.Main;


public class TongsMove extends TongsSimpleScenario {
	private static final boolean HAS_MOUTH = false;
	
	public TongsMove() {
		super(HAS_MOUTH);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Scenario (1, 3)";
	}
	@Override
	public String getDescription() {
		return "Instance of HungryFamily: most complex agent, handeling tongs. Simple events (1,3)";
	}
	@Override
	public String getAuthors() {
		return "Max";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(TongsMove.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
