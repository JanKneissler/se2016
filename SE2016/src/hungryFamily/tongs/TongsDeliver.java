/*
 *  Futter in Mund => Futter Fixed => Agent kann sich nicht bewegen => Agent l�sst Disk los => NICHT reseten
 */

package hungryFamily.tongs;

import ccc.evaluation.Main;


public class TongsDeliver extends TongsSimpleScenario {
	private static final boolean HAS_MOUTH = true;
	
	public TongsDeliver() {
		super(HAS_MOUTH);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Scenario (2, 3)";
	}
	@Override
	public String getDescription() {
		return "Instance of HungryFamily: most complex agent, handeling tongs. Harder events: Bringing the food to the Mouth (1,3)";
	}
	@Override
	public String getAuthors() {
		return "Max";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(TongsDeliver.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
