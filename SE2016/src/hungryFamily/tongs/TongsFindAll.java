package hungryFamily.tongs;


import ccc.evaluation.Main;


public class TongsFindAll extends TongsFindScenario {

	private static final boolean  HAS_GLUE = true;
	
	public TongsFindAll(){
		super(HAS_GLUE);
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Scenario (3,3)";
	}

	@Override
	public String getDescription() {
		return "Instance of HungryFamily: complex agent, very complex events (3,3)";
	}

	@Override
	public String getAuthors() {
		return "Manuela";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}



	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(TongsFindAll.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}
}
