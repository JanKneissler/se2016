package hungryFamily.tongs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import scenarios.framework.ColorIndexedDiskMaterial;
import utils.Colors;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import agent.Agent;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import hungryFamily.HungryFamilyEnvironment;

public class TongsFindScenario extends HungryFamilyEnvironment {
	private DiskType gluePoint;

	private boolean touchedFoodWithoutGlue;

	private Agent agent;
	private Tongs tongs;

	private int indexOfMergeDisk1, indexOfMergeDisk2;
	private Disk tongtip1;
	private Disk tongtip2;

	private boolean releaseFood;
	private Disk capturedDisk;

	private List <Disk> deliveredFoodDisks ;


	private static final int NUMBER_OF_AGENTS = 2;
	private static final int NUMBER_OF_FOODDISKS = 3;
	private static final int NUMBER_OF_POISONDISKS = 0;
	private static final double ENERGYGAIN_PER_ITEM = 0.01;
	private static final double ENERGYGAIN_ON_WIN = 0.1;
	private static final boolean HAS_MOUTH = true;
	private boolean HAS_GLUE;
	private static final boolean BODY_MUSTNOT_TOUCH_FOOD=true; 

	private static boolean movementConsumesEnergy = true;



	public TongsFindScenario(boolean hasGlue) {
		super(NUMBER_OF_AGENTS, NUMBER_OF_FOODDISKS, NUMBER_OF_POISONDISKS, HAS_MOUTH, hasGlue, BODY_MUSTNOT_TOUCH_FOOD);

		HAS_GLUE = hasGlue;
	}

	/**
	 * create Agents 
	 * (create more than one for tool that is not attached firmly to agent)
	 */
	@Override
	protected void createAgents() {
		deliveredFoodDisks = new ArrayList <Disk> ();

		releaseFood = false;
		capturedDisk = null;

		double rotationEnergyConsumption = 1*Math.pow(10, -11);
		Actuator actuator;
		if (movementConsumesEnergy) {
			actuator = new Mover(10, 10, 1, rotationEnergyConsumption, 0);
		} else
			actuator = new Mover(10, 10, 1, 0, 0);
		
		gluePoint = new DiskType(new Colors(1, 0.01, 1, 1, -1d));

		agent = new Agent(getEnvironment(), actuator, new ColorIndexedDiskMaterial(1, 0.01, 1, 1, 0), ScenarioUtils.uniform(new Random(), 20, 40));
		tongs = new Tongs(getEnvironment(), null, (Colors)gluePoint.getMaterial(), 3, envRandom);

		// set EventHandler for Agent
		agent.setEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				if(otherDisk instanceof Disk){

					// if agent is sticky & touches the gluePoint of the Shovel, set shovel unfixed & merge them
					if((getAgents()[0].getIsSticky()|| !HAS_GLUE) && ((Disk) otherDisk).getDiskType().getMaterial() == gluePoint.getMaterial()){
						agentData[0].getDiskComplexes()[1].setDiskNotFixed(tongtip1);
						agentData[0].getDiskComplexes()[1].setDiskNotFixed(tongtip2);
						getEnvironment().merge(getAgentDisks()[0][0], (Disk) otherDisk);		
					}

					// agents gets harm, if it touches the FoodObjects without the Shovel
					Iterator<DiskComplex> it = getEnvironment().getDiskComplexes().iterator();
					DiskComplex dc = null;
					for (int i = 0; i < NUMBER_OF_FOODDISKS; i++) {
						dc = it.next();
						if((Disk) otherDisk  == dc.getDisks().get(0)){
							touchedFoodWithoutGlue=true; // agent will be harmed at end of timestep
						}
					}			
				}
			}
		});

		indexOfMergeDisk1 = tongs.mergeDisk1;
		indexOfMergeDisk2 = tongs.mergeDisk2;

		this.setAgents(new Agent[] {agent, tongs});
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* ControlledAgendsData                                                                  */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates){
		agentData= super.getControlledAgents(aRandom, eRandom, controlledAgentStates);
		tongtip1 = agentData[0].getDiskComplexes()[1].getDisks().get(indexOfMergeDisk1);
		tongtip2 = agentData[0].getDiskComplexes()[1].getDisks().get(indexOfMergeDisk2);

		// Fix the tips of the tongs
		agentData[0].getDiskComplexes()[1].setDiskFixed(tongtip1);
		agentData[0].getDiskComplexes()[1].setDiskFixed(tongtip2);

		return agentData; 
	}

	public boolean captured(){
		int sizeOfAgentComplex = getAgentDisks()[0][0].getDiskComplex().getDisks().size();
		return (sizeOfAgentComplex > 21);
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                             		             */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		
		if(touchedFoodWithoutGlue){
			agentState[0].incHarmInflicted(0.05);
			touchedFoodWithoutGlue=false; 
		}
		
		Disk body = agentDisks[0][0];
		double posX = Math.floor(body.getX() / 100);
		double posY = Math.floor(body.getY() / 100);
		
		// if agent is located over Glue, make him sticky
		if (HAS_GLUE && posX == gluePos.getX() && posY == gluePos.getY()) {
			if(!agent.getIsSticky()){
				agentState[0].incEnergyGained(ENERGYGAIN_PER_ITEM);
				agent.setIsSticky(true);
				agentDisks[0][0].getDiskType().getMaterial().setDisplayColor(Colors.getDisplayColorByIndex(GLUE_COLOR_INDEX));
				nextEpisode();
			}
		}

		// if a disk was captured and is located over mouth, release it, then agent gains energy
		if(captured()){
			DiskMaterial foodMaterial = food[0].getDiskType().getMaterial();
			for (int i = 0; i < getAgentDisks()[0][0].getDiskComplex().getDisks().size(); i++) {
				if(getAgentDisks()[0][0].getDiskComplex().getDisks().get(i).getDiskType().getMaterial() == foodMaterial){
					capturedDisk = getAgentDisks()[0][0].getDiskComplex().getDisks().get(i);

				}
			}

			double capturedX = Math.floor(capturedDisk.getX() / 100);
			double capturedY = Math.floor(capturedDisk.getY() / 100);
			if (capturedX == mouthPos.getX() && capturedY == mouthPos.getY()) {
				releaseFood = true;
				capturedDisk.getDiskComplex().setDiskFixed(capturedDisk);
			}
			else releaseFood = false;


		}
		if (!captured() && releaseFood && Math.floor(capturedDisk.getX() / 100) == mouthPos.getX() && Math.floor(capturedDisk.getY() / 100) == mouthPos.getY()) {			
			agentState[0].incEnergyGained(ENERGYGAIN_PER_ITEM);
			shiftIntoMouth(capturedDisk);
			if(!deliveredFoodDisks.contains(capturedDisk)){
				deliveredFoodDisks.add(capturedDisk);
			}
			capturedDisk = null;
			releaseFood = false;

		}

		if(deliveredFoodDisks !=null && deliveredFoodDisks.size() == NUMBER_OF_FOODDISKS){
			for (int i = 0; i < NUMBER_OF_FOODDISKS; i++) {
				resetPosition(deliveredFoodDisks.get(i));
				deliveredFoodDisks.get(i).getDiskComplex().setDiskNotFixed(deliveredFoodDisks.get(i));
			}
			agentState[0].incEnergyGained(ENERGYGAIN_ON_WIN);
			deliveredFoodDisks = new ArrayList<Disk>();
			nextEpisode();
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: ShovelFind";
	}
	@Override
	public String getDescription() {
		return "Instance of HungryFamily: Agents merges with tongs to deliver food to mouth.";
	}
	@Override
	public String getAuthors() {
		return "Manuela, Benny";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			always(0, 0.0);
			always(1, 0.0);
			always(2, 0.0);
			always(3, 0.0);

			onKey('S', 0, -0.5, false);
			onKey('W', 0, +0.5, false);
			onKey('A', 1, .1, false);
			onKey('D', 1, -.1, false);
			onKey('K', 2, 1, false);
			onKey('L', 2, -1, false);
			onKey('M', 3, 1, false);
			onKey('N', 3, -1, false);

			setLogMessage(
					"Use the following keys to control the agent:\n" + "a,d -> left/right\n" + "w,s -> up/down\n");

			setLogMessage("Use the following keys to control the agent:\n" + "a -> turn left \n" + "d -> turn right \n"
					+ "k -> open left finger \n" + "l -> close left finger \n" + "m -> open right finger \n"
					+ "n -> close right finger \n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	@Override
	protected int determineNumControlledAgents(Random aRandom, Random eRandom) {
		return 2;
	}



}
