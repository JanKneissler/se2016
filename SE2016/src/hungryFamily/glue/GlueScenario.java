package hungryFamily.glue;

import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.interfaces.CognitiveController;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import hungryFamily.HungryFamilyEnvironment;
import utils.Colors;

public class GlueScenario extends HungryFamilyEnvironment {
	
	private static final int NUMBER_OF_AGENTS = 1;
	protected static final int NUMBER_OF_FOODDISKS = 3;
	private static final int NUMBER_OF_POISONDISKS = 3;
	
	private static boolean movementConsumesEnergy = true;

	public GlueScenario(boolean hasMouth, boolean hasGlue) {
		super(NUMBER_OF_AGENTS, NUMBER_OF_FOODDISKS, NUMBER_OF_POISONDISKS, hasMouth, hasGlue);
	}
	
	/** create Agents */
	@Override
	protected void createAgents() {
		double agentBodyRadius = ScenarioUtils.uniform(new Random(), 20, 40);
		double moveEnergyConsumption = 1*Math.pow(10, -11);
		Actuator actuator;
		
		if (movementConsumesEnergy) {
			actuator = new Mover(10, 10, 1, moveEnergyConsumption, 0);
		} else
			actuator = new Mover(10, 10, 1, 0, 0);
		
		Agent agent = new Agent(getEnvironment(), actuator, new Colors(1, 0.01, 1, 1, 0), agentBodyRadius);
		this.setAgents(new Agent[] { agent });
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: Glue (1,x)";
	}

	@Override
	public String getDescription() {
		return "Scenarios where the agent has glue feature.";
	}

	@Override
	public String getAuthors() {
		return "Manu";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			always(0, 0.0);
			always(1, 0.0);
			onKey('S', 0, -0.5, true);
			onKey('W', 0, +0.5, true);
			onKey('A', 1, .1, true);
			onKey('D', 1, -.1, true);

			setLogMessage(
					"Use the following keys to control the agent:\n" + "a,d -> left/right\n" + "w,s -> up/down\n");
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}


}
