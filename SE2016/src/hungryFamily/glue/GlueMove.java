package hungryFamily.glue;

import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import environmentalObjects.Food;

public class GlueMove extends GlueScenario {

	private static final boolean HAS_MOUTH = false;
	private static final boolean HAS_GLUE = false;

	public GlueMove() {
		super(HAS_MOUTH, HAS_GLUE);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {

		Disk agentDisk = getAgentDisks()[0][0].getDiskComplex().getDisks().get(0);
		
		// add EventHandler
		getAgentDisks()[0][0].addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				// agent glues to foodDisk but not to poisonDisk
				if (otherDisk instanceof Disk && ((Disk) otherDisk).getDiskType().equals(Food.foodType)) {
					getEnvironment().merge(agentDisk, (Disk) otherDisk);
				}
			}

		});
		
		if (allFoodcollected(agentDisk)) {
			agentDisk.getDiskComplex().splitFromNeighbors(agentDisk);
			resetFoodDisks();
		}

		super.updateAtEndOfTimeStep(agentState);
	};


	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario */
	///////////////////////////////////////////////////////////////////////////////////////////

	public boolean allFoodcollected(Disk agent) {
		return agent.getDiskComplex().getDisks().size() > NUMBER_OF_FOODDISKS;
	}

	@Override
	public String getName() {
		return "HungryFamily: GlueMove (1,0)";
	}
	@Override
	public String getDescription() {
		return "Agent with glue feature collects food. \n"
				+ "Once he has all food glued to himself, he gains energy \n"
				+ "and the food is resetted. \n"
				+ "The agent gets harmed at contact with poison,";
	}
	@Override
	public String getAuthors() {
		return "Manu";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(GlueMove.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}
}
