package hungryFamily.glue;

import java.util.Random;

import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import environmentalObjects.Food;

public class GlueFindTool extends GlueScenario {

	private static final boolean HAS_MOUTH = true;
	private static final boolean HAS_GLUE = true;

	public GlueFindTool() {
		super(HAS_MOUTH, HAS_GLUE);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* ControlledAgentData                                                                   */
	///////////////////////////////////////////////////////////////////////////////////////////	
	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates){
		
		ControlledAgentData[] agentData= super.getControlledAgents(aRandom, eRandom, controlledAgentStates);
		Disk agentDisk = getAgentDisks()[0][0].getDiskComplex().getDisks().get(0);

		// add EventHandler, so food sticks to Agent only if he is sticky
		getAgentDisks()[0][0].addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
				if (otherDisk instanceof Disk) {
					// if sticky: glue to food Disk
					if (getAgents()[0].getIsSticky() && ((Disk) otherDisk).getDiskType().equals(Food.foodType) ) {
						getEnvironment().merge(agentDisk, (Disk) otherDisk);
					} else if (!getAgents()[0].getIsSticky() && ((Disk) otherDisk).getDiskType().equals(Food.foodType) ) {
						controlledAgentStates[0].incHarmInflicted(0.0001);
					}
				}
			}
		});
		return agentData; 
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* updateAtEndOfTimeStep                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentState) {
		collectFoodInMouth(agentState, true);
		super.updateAtEndOfTimeStep(agentState);
	};

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "HungryFamily: GlueFindTool (1,2)";
	}
	@Override
	public String getDescription() {
		return "Agent acquires glue feature by traversing a gluearea. \n"
				+ "The gluey agent collects food in mouth. \n"
				+ "Once all food is collected, scene is resetted. \n"
				+ "The agent gets harmed at contact with poison,";
	}
	@Override
	public String getAuthors() {
		return "Manu";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(GlueFindTool.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}

}
