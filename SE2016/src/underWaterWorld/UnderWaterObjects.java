package underWaterWorld;

import java.util.Random;

import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.ObjectConstructor;
import diskworld.linalg2D.Point;
import utils.Colors;

public class UnderWaterObjects {


	static void createEnvObjects(Environment environment, Random random,
			int agentCount, int agentLength, int jointCount) {

		DiskType foodType = new DiskType(new DiskMaterial(0.01, 0.02, 0.01,
				0.01, Colors.LIGHT_CORAL));
		DiskType collectorType = new DiskType(new DiskMaterial(0.01, 0.02,
				0.01, 0.01, Colors.HEATHERED_GRAY));
		ObjectConstructor foodConstructor = environment
				.createObjectConstructor();
		ObjectConstructor collectorConstructor = environment
				.createObjectConstructor();
		foodConstructor.setRoot(UnderWaterSzenario.FOOD_RADIUS, true, foodType);
		collectorConstructor.setRoot(UnderWaterSzenario.COLLECTOR_RADIUS, true,
				collectorType);

		boolean wasPlaced = false;
		Point posF = foodPos(random, agentCount, agentLength, jointCount);
		double foodX = posF.getX();
		double foodY = posF.getY();
		// create food (gives reward if placed in foodbasket)
		double orientation = 0.0; // orientation does not matter
		double scale = 1.0;
		do {
			wasPlaced = foodConstructor.createObject(foodX, foodY, orientation,
					scale);
		} while (!wasPlaced);

		// create food collector
		Point posC = collectorPos(random, agentCount, agentLength, jointCount);
		double collX = posC.getX();
		double collY = posC.getY();
		do {
			wasPlaced = collectorConstructor.createObject(collX, collY,
					orientation, scale);
		} while (!wasPlaced);
	}

	private static int posMostRightAlga(int agentCount) {
		return UnderWaterSzenario.agentCount * 1000
				/ (UnderWaterSzenario.agentCount + 1);
	}

	private static int posMostLeftAlga(int agentCount) {
		return 1000 / (UnderWaterSzenario.agentCount + 1);
	}

	private static AgentData agentData(int agentLength, int jointCount) {
		return AlgaAgent.reachablePositions(agentLength, jointCount);
	}

	static Point foodPos(Random rand, int agentCount, int agentLength,
			int jointCount) {
		double minX = posMostRightAlga(agentCount) + AlgaAgent.RADIUSDISK
				+ UnderWaterSzenario.FOOD_RADIUS + 1;
		double maxX = 1000 - UnderWaterSzenario.FOOD_RADIUS - 1;
		double minY = 3 * AlgaAgent.RADIUSDISK;
		double maxY = agentData(agentLength, jointCount).getHeight() - 4;
		double foodX;
		double foodY;
		do {
			foodX = minX + (maxX - minX) * rand.nextDouble();
			foodY = minY + (maxY - minY) * rand.nextDouble();
		} while (!possibleFoodPosition(foodX, foodY, agentCount, agentLength,
				jointCount));

		return new Point(foodX, foodY);
	}

	static Point collectorPos(Random rand, int agentCount, int agentLength,
			int jointCount) {
		double minX = UnderWaterSzenario.COLLECTOR_RADIUS + 1;
		double maxX = posMostLeftAlga(agentCount)
				- (AlgaAgent.RADIUSDISK + UnderWaterSzenario.FOOD_RADIUS + 1);
		double minY = 3 * AlgaAgent.RADIUSDISK;
		double maxY = agentData(agentLength, jointCount).getHeight() - 4;
		double collX;
		double collY;
		do {
			collX = minX + (maxX - minX) * rand.nextDouble();
			collY = minY + (maxY - minY) * rand.nextDouble();
		} while (!possibleCollectorPosition(collX, collY, agentCount,
				agentLength, jointCount));
		return new Point(collX, collY);
	}

	private static boolean possibleFoodPosition(double x, double y,
			int agentCount, int agentLength, int jointCount) {
		double radius = Math.sqrt(Math.pow(x - posMostRightAlga(agentCount), 2)
				+ Math.pow(y - 3 * AlgaAgent.RADIUSDISK, 2));
		return (radius >= agentData(agentLength, jointCount).getMinRadius() && radius <= agentData(
				agentLength, jointCount).getMaxRadius()
				+ UnderWaterSzenario.FOOD_RADIUS);
	}

	private static boolean possibleCollectorPosition(double x, double y,
			int agentCount, int agentLength, int jointCount) {
		double radius = Math.sqrt(Math.pow(x - posMostLeftAlga(agentCount), 2)
				+ Math.pow(y - 3 * AlgaAgent.RADIUSDISK, 2));
		return (radius >= agentData(agentLength, jointCount).getMinRadius() && radius <= agentData(
				agentLength, jointCount).getMaxRadius()
				+ UnderWaterSzenario.COLLECTOR_RADIUS);
	}

}
