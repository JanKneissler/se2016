package underWaterWorld;

import java.io.PrintStream;

import ccc.interfaces.CognitiveController;

public class AlgaController implements CognitiveController {

	static int agentCount = UnderWaterSzenario.agentCount; // change number of
															// alga agents here,
															// maximum 10
	// agents
	static int agentLength = UnderWaterSzenario.agentLength; // change length of
																// alga agents
																// here
	static int jointCount = UnderWaterSzenario.jointCount; // change number of
															// joints and disks
															// here, minimum 1

	private String[] sensors;
	private String[] actuators;
	private double[] oldSonsorValues;
	private double deliverPoint;
	private int counter;
	private int currentArm;
	private double lastEnergy;

	int time, lastTime, dur;

	private enum State {
		Prepare, GetFood, Erect, Wiggle, Give, Deliver
	};

	private State currentState;

	@Override
	public void doTimeStep(double energyLevel, double harmLevel, double[] sensorValues, double[] actuatorValues) {

		time++;

		switch (currentState) {
		case Wiggle:
			dur = 200;
			for (int i = 0; i < actuatorValues.length; i++) {
				int arm = i / jointCount;
				int link = i % jointCount;
				actuatorValues[i] = 10.0 / ((double) agentLength * (double) jointCount) * Math.sin(time * 0.08 + link + arm % 2) + moveToLine(
						sensorValues[arm * agentLength * 2 + 4],
						sensorValues[arm * agentLength * 2 + 4 + (link + 1) * (agentLength - 2) / jointCount * 2]);
			}
			if (time - lastTime > dur) {
				currentState = State.Prepare;
				lastTime = time;
				deliverPoint = sensorValues[0] - 1 / (double) (agentCount + 1);
			}
			break;
		case Erect:
			dur = 200;
			for (int i = 0; i < actuatorValues.length; i++) {
				int arm = i / jointCount;
				int link = i % jointCount;
				actuatorValues[i] = moveToLine(sensorValues[arm * agentLength * 2 + 4],
						sensorValues[arm * agentLength * 2 + 4 + (link + 1) * (agentLength - 2) / jointCount * 2]);
			}
			if (time - lastTime > dur) {
				currentState = State.Give;
				lastTime = time;
				deliverPoint = sensorValues[0] - 1 / (double) (agentCount + 1);
				currentArm = 0;
				while (!inRange(sensorValues[0], sensorValues[4 + (currentArm + 1) * agentLength * 2 - 2], 0.1))
					currentArm++;
			}
			break;
		case Give:

			actuatorValues[currentArm * jointCount] = 0.2 * moveToLine(deliverPoint,
					sensorValues[(currentArm + 1) * agentLength * 2 - 2 + 4]);
			actuatorValues[(currentArm + 1) * jointCount - 1] = 5 * moveToLine(deliverPoint-0.1,
					sensorValues[(currentArm + 1) * agentLength * 2 - 2 + 4]);

			if (currentArm != 0) {
				actuatorValues[(currentArm - 1) * jointCount] = 0.2 * moveToLine(deliverPoint,
						sensorValues[currentArm * agentLength * 2 - 2 + 4]);
				actuatorValues[currentArm * jointCount - 1] = - 0.2 * moveToLine(sensorValues[1]-0.2,
						sensorValues[currentArm * agentLength * 2 - 2 + 4 + 1]);
			}
			
			if (inRange(sensorValues[0], sensorValues[2], 0.09)) {
				currentState = State.Deliver;
			}

			if (time - lastTime > 40) {
				currentState = State.Erect;
				lastTime = time;
			}

			break;
		case Prepare:
			if (inRange(sensorValues[agentCount * agentLength * 2 + 4 - 1], sensorValues[1] - 0.03, 0.01))
				currentState = State.GetFood;

			for (int i = 0; i < (agentCount - 1) * jointCount; i++) {
				int arm = i / jointCount;
				int link = i % jointCount;
				actuatorValues[i] = moveToLine(sensorValues[arm * agentLength * 2 + 4],
						sensorValues[arm * agentLength * 2 + 4 + (link + 1) * (agentLength - 2) / jointCount * 2]);
			}

			// joints der letzten alge
			for (int i = (agentCount - 1) * jointCount; i < actuatorValues.length; i++) {
				actuatorValues[i] = 0.5 - ((i - ((agentCount - 1) * jointCount - 1)) / (double) jointCount);
			}

			break;
		case GetFood:

			if (!inRange(sensorValues[0], oldSonsorValues[0], 0.005)) {
				currentState = State.Erect;
				lastTime = time;
			}

			for (int i = (agentCount - 1) * jointCount + 1; i < actuatorValues.length; i++) {
				actuatorValues[i] = -moveToLine(sensorValues[1], sensorValues[sensorValues.length - 1]);
			}
			actuatorValues[(agentCount - 1) * jointCount] = -0.5;

			break;

		case Deliver:

			actuatorValues[0] = -0.5;
			for (int i = 1; i < jointCount; i++) {
				actuatorValues[i] = 1.4 / (jointCount - 1);
			}

			for (int i = jointCount; i < actuatorValues.length; i++) {
				int arm = i / jointCount;
				int link = i % jointCount;
				actuatorValues[i] = moveToLine(sensorValues[arm * agentLength * 2 + 4],
						sensorValues[arm * agentLength * 2 + 4 + (link + 1) * (agentLength - 2) / jointCount * 2]);
			}

			break;
		}

		if (energyLevel - lastEnergy != 0 && energyLevel - lastEnergy != 0.5) {
			currentState = State.Wiggle;
			lastTime = time;
		}

		lastEnergy = energyLevel;

		// printActivity(sensorValues);
		oldSonsorValues = new double[sensorValues.length];
		for (int i = 0; i < sensorValues.length; i++) {
			oldSonsorValues[i] = sensorValues[i];
		}
	}

	/*
	 * Prints everything that moves fast
	 */
	public void printActivity(double[] sensorValues) {
		if (oldSonsorValues != null) {
			for (int i = 0; i < sensorValues.length; i++) {
				if (!inRange(oldSonsorValues[i], sensorValues[i], 0.005)) {
				}
			}
		}
	}

	public boolean inRange(double a, double b, double range) {
		return b + range > a && b - range < a;
	}

	private double moveToLine(double line, double current) {
		return (current - line) * 10;
	}

	@Override
	public void nextEpisode(String[] sensors, String[] actuators) {
		this.sensors = sensors;
		this.actuators = actuators;
		currentState = State.Prepare;
		time = 0;
		lastTime = 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "AlgaController";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String getAuthors() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String getVersion() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public void provideLogStream(PrintStream logStream) {

	}

	@Override
	public void terminated() {
		// TODO Auto-generated method stub

	}

}
