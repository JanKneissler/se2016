package underWaterWorld;

public class AgentData {

	private double minRadius;
	private double maxRadius;
	private double height;

	public AgentData(double minRadius, double maxRadius, double height) {
		this.minRadius = minRadius;
		this.maxRadius = maxRadius;
		this.height = height;
	}

	public double getMinRadius() {
		return this.minRadius;
	}

	public double getMaxRadius() {
		return this.maxRadius;
	}

	public double getHeight() {
		return this.height;
	}

}
