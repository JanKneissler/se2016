package underWaterWorld;

import java.util.Iterator;
import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.AgentConstructor;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.evaluation.Main;
import ccc.interfaces.AgentState;
import ccc.interfaces.CognitiveController;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.Environment;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.FloorCellType;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;
import utils.Colors;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;

public class UnderWaterSzenario extends GlobalInformationFixedDiskNumScenario {
	private static final double FLOOR_TILE_SIZE = 100;
	private static final double ENERGY_GAIN_PER_FOOD_ITEM = 0.01;
	private static final double ENERGY_LOSS = 0.0001;

	static final double FOOD_RADIUS = 20;
	static final double COLLECTOR_RADIUS = 35;

	private Random random;

	//values are static to enable access for AlgaController
	static int agentCount;
	static int agentLength;
	static int jointCount;

	private Agent[] agents;
	private Disk[][] agentDisks;
	private DiskComplex[] agentComplexes;
	private Disk[] disks;

	private Disk foodDisk;
	private Disk collectorDisk;

	public UnderWaterSzenario() {
		super(false, false, FLOOR_TILE_SIZE);
	}

	@Override
	public String getName() {
		return "UnderWaterWorld";
	}

	@Override
	public String getDescription() {
		return "Underwater World, consists of several alga agents. \n"
				+ "they are supposed to carry food from one side of the tank \n"
				+ "to their food collector on the other side \n"
				+ "in order to get a reward.";
	}

	@Override
	public String getAuthors() {
		return "Flavia + Lilian + Fabian";
	}

	@Override
	public String getVersion() {
		return "2.0";
	}

	private static int getRandom(Random rand, int from, int to) {
		return rand.nextInt(to - from + 1) + from;
	}

	@Override
	protected void initialize(Environment environment, Random random) {
		this.random = random; // environmental seed becomes available in any
								// method

		agentCount = getRandom(random, 1, 8);
		agentLength = getRandom(random, 3, 40);
		jointCount = getRandom(random, 1, agentLength - 2);

		environment.getPhysicsParameters().setGravityModel(
				new ConstantGravityModel(0));
		getEnvironment().getFloor().fill(
				new FloorCellType(0.03, Colors.DEEP_SEA));

		UnderWaterObjects.createEnvObjects(environment, random, agentCount,
				agentLength, jointCount);

		Iterator<DiskComplex> iterator = getEnvironment().getDiskComplexes()
				.iterator();
		foodDisk = iterator.next().getDisks().get(0);
		foodDisk.addEventHandler(new CollisionEventHandler() {
			@Override
			public void collision(final CollidableObject otherDisk,
					Point collisionPoint, double exchangedImpulse) {
				if (otherDisk instanceof Disk) {
					if (((Disk) otherDisk).getDiskComplex().getDisks().size() >= agentLength
							&& ((Disk) otherDisk)
									.getDiskType()
									.getMaterial()
									.getDisplayColor()
									.equals(AlgaAgent.stickyType.getMaterial()
											.getDisplayColor())) {
						// food object is merged with alga
						if (foodDisk.getDiskComplex().getDisks().size() > 1) {
							if (((Disk) otherDisk).getDiskComplex().getDisks()
									.get(0).getX() < foodDisk.getDiskComplex()
									.getDisks().get(0).getX()) {
								foodDisk.getDiskComplex().splitFromNeighbors(
										foodDisk);
								getEnvironment().merge(foodDisk,
										(Disk) otherDisk);
							}
						} else {
							getEnvironment().merge(foodDisk, (Disk) otherDisk);
							if (foodDisk.isMarkedFixed()) {
								foodDisk.getDiskComplex().setDiskNotFixed(
										foodDisk);
							}
						}
					}
				}
			}
		});

		collectorDisk = iterator.next().getDisks().get(0);
		collectorDisk.setZLevel(-1);
	}

	public ControlledAgentData[] getControlledAgents(Random aRandom,
			Random eRandom, AgentState[] controlledAgentStates) {

		// create DiskType and build agent-complex

		double initialEnergyLevel = 0.5; // start with half energy
		controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
		ControlledAgentData[] agentData = new ControlledAgentData[agentCount];
		// ControlledAgentData[] agents = new ControlledAgentData[agentCount];
		AlgaAgent alga = null;
		agents = new Agent[agentCount];
		agentDisks = new Disk[agentCount][];
		agentComplexes = new DiskComplex[agentCount];

		// flexible length of agents, joints and agentLength
		for (int i = 0; i < agents.length; i++) {
			alga = new AlgaAgent(getEnvironment(), agentLength, jointCount);
			agents[i] = alga;
			agentData[i] = alga.createControlledAgent((i + 1) * 1000
					/ (agents.length + 1), AlgaAgent.RADIUSDISK + 1,
					Math.toRadians(90), 1);
			agentComplexes[i] = agentData[i].getDiskComplex();
			agentDisks[i] = new Disk[alga.getSize()];
			for (int j = 0; j < agents[i].getSize(); j++) {
				agentDisks[i][j] = agentData[i].getDiskComplex().getDisks()
						.get(j);
				// only if an EventHandler is defined add it to this disk
				if (agents[i].getEventHandler() != null)
					agentDisks[i][j].addEventHandler(agents[i]
							.getEventHandler());
			}
		}

		// create all agents
		agentData = new ControlledAgentData[] { AgentConstructor
				.createControlledAgentWithMultipleDiskComplexes(agentComplexes) };
		// set all disks
		this.setDisks();

		return agentData;
	}

	/** should only be invoked once, when the scenario is created */
	private void setDisks() {
		int sum = 0;
		for (int i = 0; i < agentDisks.length; i++) {
			sum += agentDisks[i].length;
		}
		disks = new Disk[sum + 2];
		int n = 0;
		disks[n++] = foodDisk;
		disks[n++] = collectorDisk;
		for (int i = 0; i < agentDisks.length; i++) {
			for (int j = 0; j < agentDisks[i].length; j++) {
				disks[n] = agentDisks[i][j];
				n++;
			}
		}
		this.setDisks(disks);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Called every time step, put necessary update code (besides the
	 * DiskWorldPhysics)
	 */
	// /////////////////////////////////////////////////////////////////////////////////////////

	protected void updateAtEndOfTimeStep(AgentState[] agentState) {

		boolean samePosition = (foodDisk.overlaps(collectorDisk));

		if (samePosition) {
			foodDisk.getDiskComplex().splitFromNeighbors(foodDisk);
			Point pos = UnderWaterObjects.foodPos(random, agentCount,
					agentLength, jointCount);
			double foodX = pos.getX();
			double foodY = pos.getY();
			getEnvironment().canTeleport(foodDisk, foodX, foodY, 0);
			foodDisk.getDiskComplex().setDiskFixed(foodDisk);
			getControlledAgentStates()[0]
					.incEnergyGained(ENERGY_GAIN_PER_FOOD_ITEM);
		}

		for (int i = 0; i < agentDisks.length; i++) {
			for (int j = 0; j < agentDisks[i].length; j++) {

				boolean samePositionAgent = (agentDisks[i][j]
						.overlaps(collectorDisk));
				if (samePositionAgent) {
					(getControlledAgentStates()[0])
							.incHarmInflicted(ENERGY_LOSS);
				}

			}

		}
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Called at the end of the simulation to determine the score */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public double getScore() {
		return 0;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends
			KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			// deprecated, use gui controller instead
		}
	}

	// start scenario
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	@Override
	public Class<? extends CognitiveController> getReferenceControllerClass() {
		return AlgaController.class;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario */
	// /////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = true;
		boolean useGUIController = false;
		Main.addScenario(UnderWaterSzenario.class, useReferenceController,
				false, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });

	}

}
