package underWaterWorld;

import agent.Agent;
import diskworld.DiskMaterial;
import diskworld.DiskType;
import diskworld.Environment;
import diskworld.actions.JointActionType;
import diskworld.actions.JointActionType.ActionType;
import diskworld.actions.JointActionType.ControlType;
import diskworld.linalg2D.Point;
import utils.Colors;

/**
 * AlgaAgent consists of one arm with joint disks and is fixed on the ground.
 * 
 * @author Fabian, Lilian & Flavia
 */
public class AlgaAgent extends Agent {

	public static DiskType armType = new DiskType(
			DiskMaterial.METAL.withColor(Colors.ALGA_GREEN));
	public static DiskType stickyType = new DiskType(
			DiskMaterial.METAL.withColor(Colors.ALGA_JOINT));
	public static final double RADIUSDISK = 20;
	public static final double ROTATION_VALUE = Math.PI / 4;

	public AlgaAgent(Environment env, int diskCount, int jointCount) {
		super(env);
		boolean fixed = true;
		this.setRoot(RADIUSDISK, fixed, armType);
		this.createJointType("joint",
				DiskMaterial.METAL.withColor(Colors.DARK_ALGA),
				new JointActionType(0.5, ControlType.CHANGE, ActionType.SPIN));

		diskCount -= 2; // body disks with joints without root and sticky disk
		int distance = (diskCount + 1) / jointCount;
		double radius;
		for (int i = 0; i < diskCount; i++) {
			radius = RADIUSDISK * Math.pow(0.95, i);
			if (i % distance == 0 && jointCount > 0) { // guarantees set number
														// of joints
				int elbowId = this.addNewItem(-1, Math.toRadians(0), 0, radius,
						"joint");
				this.setActionRange(elbowId, ActionType.SPIN, (-1)
						* ROTATION_VALUE, ROTATION_VALUE);
				jointCount--;
			} else {
				this.addNewItem(-1, Math.toRadians(0), 0, radius, armType);
			}
		}
		this.addNewItem(-1, Math.toRadians(0), 0,
				RADIUSDISK * Math.pow(0.95, diskCount), stickyType);

		env.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(
				0.0);
	}

	/**
	 * @return parameters which set limits for reachable positions of glue disk
	 */
	public static AgentData reachablePositions(int diskCount, int jointCount) {
		diskCount -= 2; // body disks with joints without root and sticky disk
		int distance = (diskCount + 1) / jointCount;

		// Coordinates of rotation axis of each joint in initial position
		double[] jointPositions = new double[jointCount + 1]; // joints and top
																// disk
		double yPosInAlga = 2 * RADIUSDISK; // root is already set
		double radius;
		int index = 0; // fill jointPositions
		for (int i = 0; i < diskCount; i++) {
			radius = RADIUSDISK * Math.pow(0.95, i);
			yPosInAlga += 2 * radius;
			if (i % distance == 0 && jointCount > 0) {
				jointPositions[index] = yPosInAlga - radius;
				index++;
				jointCount--;
			}
		}
		double height = yPosInAlga + 2 * RADIUSDISK * Math.pow(0.95, diskCount);

		Point[] joints = new Point[jointPositions.length];
		for (int i = 0; i < joints.length - 1; i++) {
			joints[i] = new Point(0, jointPositions[i]);
		}
		joints[joints.length - 1] = new Point(0, height);

		// new position of the disk at the top if each joint is at max. angle
		// first joint stays in initial position
		for (int i = 0; i < jointPositions.length; i++) {
			// calculate positions if joint i is rotation axis
			rotateCoordinates(joints, i);
		}

		double topX = joints[joints.length - 1].getX();
		double topY = joints[joints.length - 1].getY();
		double minRadius = Math.sqrt(Math.pow(topX, 2)
				+ Math.pow(topY - 3 * RADIUSDISK, 2));

		double maxRadius = height - 3 * RADIUSDISK;
		return new AgentData(minRadius, maxRadius, height);

	}

	private static void rotateCoordinates(Point[] joints, int firstJoint) {
		double angle = ROTATION_VALUE;

		double c1 = joints[firstJoint].getX(); // axis of rotation
		double c2 = joints[firstJoint].getY();

		for (int i = firstJoint + 1; i < joints.length; i++) {
			double x1 = joints[i].getX(); // points to rotate
			double x2 = joints[i].getY();
			double newX = c1 + (x1 - c1) * Math.cos(angle) - (x2 - c2)
					* Math.sin(angle);
			double newY = c2 + (x1 - c1) * Math.sin(angle) + (x2 - c2)
					* Math.cos(angle);
			joints[i] = new Point(newX, newY);
		}
	}

}
