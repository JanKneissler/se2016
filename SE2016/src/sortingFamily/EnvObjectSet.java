package sortingFamily;

import java.util.ArrayList;
import java.util.Random;

import diskworld.Environment;
import environmentalObjects.BlueCircle;
import environmentalObjects.EnvObject;
import environmentalObjects.GreenSquare;
import environmentalObjects.OrangePair;
import environmentalObjects.PinkCorner;
import environmentalObjects.RedTriangle;
import environmentalObjects.YellowStar;

/**
 * EnvObjectSet is an arrayList that contains
 * an array for each possible type of environmental object
 * which holds (numberOfObjectsPerType x entities) of that type
 */
public class EnvObjectSet extends ArrayList<EnvObject[]> {

	private static final long serialVersionUID = 1L;

	public EnvObjectSet(Environment env, Random envRandom, int numberOfObjectsPerType) {
		
		// BlueCircle
		EnvObject[] blueCircles = new EnvObject[numberOfObjectsPerType];
		for (int i = 0; i < blueCircles.length; i++) {
			blueCircles[i] = new BlueCircle(env, envRandom);
		}
		this.add(blueCircles);
		
		// GreenSquare
		EnvObject[] greenSquares = new EnvObject[numberOfObjectsPerType];
		for (int i = 0; i < greenSquares.length; i++) {
			greenSquares[i] = new GreenSquare(env, envRandom);
		}
		this.add(greenSquares);
		
		// OrangePair
		EnvObject[] orangePairs = new EnvObject[numberOfObjectsPerType];
		for (int i = 0; i < orangePairs.length; i++) {
			orangePairs[i] = new OrangePair(env, envRandom);
		}
		this.add(orangePairs);
		
		// PinkCorner
		EnvObject[] pinkCorners = new EnvObject[numberOfObjectsPerType];
		for (int i = 0; i < pinkCorners.length; i++) {
			pinkCorners[i] = new PinkCorner(env, envRandom);
		}
		this.add(pinkCorners);
		
		// RedTriangle
		EnvObject[] redTriangles = new EnvObject[numberOfObjectsPerType];
		for (int i = 0; i < redTriangles.length; i++) {
			redTriangles[i] = new RedTriangle(env, envRandom);
		}
		this.add(redTriangles);
		
		// YellowStar
		EnvObject[] yellowStars = new EnvObject[numberOfObjectsPerType];
		for (int i = 0; i < yellowStars.length; i++) {
			yellowStars[i] = new YellowStar(env, envRandom);
		}
		this.add(yellowStars);
		
	}
	
}
