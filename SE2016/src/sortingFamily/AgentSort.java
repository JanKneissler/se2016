package sortingFamily;

import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.KeyStrokeInteractiveController;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.evaluation.Main;
import ccc.interfaces.CognitiveController;
import diskworld.actuators.Mover;
import diskworld.interfaces.Actuator;
import utils.Colors;

public class AgentSort extends SortingFamilyEnvironment {

	private static int numberOfAgents = 1;
	
	private static boolean bucketDisksOn = true;
	private static boolean floorBucketsOn = true;

	private static boolean movementConsumesEnergy = true;
	private static double energyGainPerItem = 0.001;
	private static double energyGainOnWin = 0.05;

	public AgentSort() {
		super(numberOfAgents, bucketDisksOn, floorBucketsOn, energyGainPerItem, energyGainOnWin);
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* agent                                                                                 */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	protected void createAgents(Random agentRandom) {
		// new BodyRadius for each AgentSeed // bigger agent can carry more objects at once
		double agentBodyRadius = ScenarioUtils.uniform(agentRandom, 15, 30);
		
		double maxBackwardDistance = 8;
		double maxForwardDistance = 8;
		double maxRotationValue = 8;
		double moveEnergyConsumption = 0;
		if (movementConsumesEnergy) {
			moveEnergyConsumption = 1*Math.pow(10, -9);
		}
		double rotationEnergyConsumption = 0;
		
		Actuator actuator = new Mover(maxBackwardDistance, maxForwardDistance, maxRotationValue, moveEnergyConsumption, rotationEnergyConsumption);
		
		Colors diskMaterial = new Colors(1, 0.01, 1, 1, 0);
		Agent agent = new Agent(getEnvironment(), actuator, diskMaterial, agentBodyRadius);
		this.setAgents(new Agent[] { agent });
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario                                                        */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "Sorting Family";
	}
	@Override
	public String getDescription() {
		return "This scenario contains objects differing in their color/type. \n" + 
				"There is a bucket area for each of the colors/types. \n" + 
				"The agent gets rewarded for each object he carries to the right bucket. \n" +
				"The buckets can be visible to the agent or not. \n" +
				"number of types:  " + numberOfTypes + "\n" + 
				"number of objects per type: " + numberOfObjectsPerType + "\n";
	}
	@Override
	public String getAuthors() {
		return "blank";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Provide interactive controller: control the agent by keyboard          				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static class InteractiveController extends KeyStrokeInteractiveController {
		@Override
		public void addKeyEvents() {
			always(0, 0.0);
			always(1, 0.0);

			//onKey(char keyChar, int actuatorIndex, double value, boolean increment);
			onKey('S', 0, -1, true);
			onKey('W', 0, +1, true);
			onKey('A', 1, .02, false);
			onKey('D', 1, -.02, false);

			setLogMessage("Use the following keys to control the agent:\n" +
					"a -> turn left \n" +
					"d -> turn right \n" + 
					"w -> forward \n" +
					"s -> backward" );
		}
	}

	@Override
	public Class<? extends CognitiveController> getInteractiveControllerClass() {
		return InteractiveController.class;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* Main method: Test this scenario           							 				 */
	///////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String args[]) {
		boolean useReferenceController = false;
		boolean useKeyboardController = true;
		boolean useGUIController = false;
		Main.addScenario(AgentSort.class, useReferenceController, useKeyboardController, useGUIController);
		// start evaluation main in interactive mode
		Main.main(new String[] { "-i" });
	}
}
