package sortingFamily;

import java.awt.Color;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import agent.Agent;
import ccc.DiskWorldScenario.ControlledAgentData;
import ccc.DiskWorldScenario.ScenarioUtils;
import ccc.interfaces.AgentState;
import diskworld.Disk;
import diskworld.DiskComplex;
import diskworld.Environment;
import diskworld.environment.ConstantGravityModel;
import diskworld.environment.Floor;
import diskworld.environment.FloorCellType;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.linalg2D.Point;
import environmentalObjects.Bucket;
import environmentalObjects.EnvObject;
import scenarios.framework.ColorIndexedDiskMaterial;
import scenarios.framework.GlobalInformationFixedDiskNumScenario;


public abstract class SortingFamilyEnvironment extends GlobalInformationFixedDiskNumScenario {

	private static final double FLOOR_TILE_SIZE = 100;	// 10 x 10 floor tiles

	protected int numberOfTypes;						// 0 < numberOfTypes <= 6 (s. environmentalObjects)
	protected int numberOfObjectsPerType;				// 0 < numberOfObjectsPerType

	private double initialEnergyLevel = 0.5;
	private double energyGainPerItem;
	private double energyGainOnWin; 					// once all items have been sorted once
	private int sortedItems = 0; 						// count items that have been sorted

	// AGENTS
	private Agent[] agents;
	protected ControlledAgentData agentData[];
	protected Disk[][] agentDisks; 						// 2D: agentDisks for each agent

	// BUCKETS
	private HashMap<Double,Point> bucketPos; 			// Coordinates of all buckets mapped to according colorIndex
	private boolean bucketDisksOn;						// whether to create a disk for each bucket in z-level
	private boolean floorBucketsOn;						// whether buckets are to be represented on floor
	private static HashMap<Double,FloorCellType> floorCellTypes = new HashMap<Double,FloorCellType>();
														// maps each bucket colorIndex to a floorCellType
														// static so that floorCellTypes are reused for all seeds
	// OBJECTS
	private EnvObjectSet envObjectSet;					// contains EnvObjects of each possible type

	private HashMap<Disk, EnvObject> allDisksMapped;	// all disks mapped to the EnvObject they instantiate
	
	private Disk[] disks;								// contains all disks in scenario
	protected static Random envRandom;
	private int bucketZLevel = -1;

	
	public SortingFamilyEnvironment(int numberOfAgents, boolean bucketDisksOn, boolean floorBucketsOn, double energyGainPerItem, double energyGainOnWin) {
		super(false, true, FLOOR_TILE_SIZE);
		this.agents = new Agent[numberOfAgents];
		this.floorBucketsOn = floorBucketsOn;
		this.bucketDisksOn = bucketDisksOn;
		this.energyGainPerItem = energyGainPerItem;
		this.energyGainOnWin = energyGainOnWin;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* information about the scenario-family */
	///////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String getName() {
		return "SortingFamilyEnvironment";
	}
	@Override
	public String getDescription() {
		return "This scenario contains objects differing in their color/type. \n " + 
				"There is a bucket area for each of the colors/types. \n " + 
				"The agent is sticky and thus can carry objects around. \n " +
				"He gets rewarded for each object that is in the right bucket. \n" +
				"The buckets can be visible to the agent or not.";
	}
	@Override
	public String getAuthors() {
		return "blank";
	}
	@Override
	public String getVersion() {
		return "1.0";
	}
	@Override
	public double getScore() {
		return 0;
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	/* initialize the environment */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void initialize(Environment environment, Random random) {
		envRandom = random;

		// choose random numberOfTypes and numberOfObjectsPerType
		numberOfTypes = ScenarioUtils.uniform(random, 1, 6);				// 0 < numberOfTypes <= 6
		numberOfObjectsPerType = ScenarioUtils.uniform(random, 1, 4);		// 0 < numberOfObjectsPerType

		// EnvObjectSet: arrayList of arrays for all objects
		envObjectSet = new EnvObjectSet(environment, random, numberOfObjectsPerType);
		Collections.shuffle(envObjectSet, random); // shuffle order of types
		
		// generate Positions for buckets
		bucketPos = new HashMap<Double, Point>();
		generateBucketPositions();

		// ----------------------------------------------------------------
		// Floor
		if (floorCellTypes.isEmpty()) {
			floorCellTypes = generateFloorCellTypes();
		}
		paintFloor();
		
		// ----------------------------------------------------------------
		// Bucket and Object Construction and allDisksMapped
		
		// store all disks mapped to the EnvObject their DiskComplex represents/instantiates
		allDisksMapped = new HashMap<Disk,EnvObject>();

		// BUCKETS
		if (bucketDisksOn) {
			for (int i = 0; i < numberOfTypes; i++) {
				double colorIndex = envObjectSet.get(i)[0].getColorIndex();
				Bucket b = new Bucket(environment, random, colorIndex);
				
				// 100: zoomFactor from coordinates to floor
				double posx = (bucketPos.get(colorIndex).x * 100) + Bucket.getRadius()+1 ;
				double posy = (bucketPos.get(colorIndex).y * 100) + Bucket.getRadius()+1;
				
				DiskComplex bucket = b.createDiskComplex(posx, posy, 0, 1.0);
				bucket.getDisks().get(0).setZLevel(bucketZLevel);
				
				// store all disks of bucket DiskComplex mapped to EnvObject "Bucket"
				for (Disk disk : bucket.getDisks()) {
					allDisksMapped.put(disk, b);
				}
			}
		}

		// OBJECTS
		for (int i = 0; i < numberOfTypes; i++) { // Types
			EnvObject[] envObject = envObjectSet.get(i);
			for (int j = 0; j < numberOfObjectsPerType; j++) { // ObjectsPerType
				boolean objectWasPlaced = false;
				DiskComplex object;
				do {
					object = envObject[j].createDiskComplex();
					objectWasPlaced = (object != null);
				} while(!objectWasPlaced);
				
				// store alls disks of the object DiskComplex mapped to equivalent EnvObject
				for (Disk disk : object.getDisks()) {
					allDisksMapped.put(disk, envObject[j]);					
				}
			}
		}

		// ----------------------------------------------------------------
		// Set shiftResistance for joints and gravity model
		environment.getPhysicsParameters().setShiftResistanceEnergyConsumptionFactor(0.0);
		environment.getPhysicsParameters().setGravityModel(new ConstantGravityModel(0.0));

	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* create agent */
	///////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected ControlledAgentData[] getControlledAgents(Random aRandom, Random eRandom, AgentState[] controlledAgentStates) {

		// call method from inheriting scenario class that defines all agents
		createAgents(aRandom);

		// initialize agentDisks[][] // as 2D-Array for the possibility of more complex agents
		agentDisks = new Disk[agents.length][];
		for (int i = 0; i < agents.length; i++) {
			agentDisks[i] = new Disk[agents[i].getSize()];
		}

		agentData = new ControlledAgentData[agents.length];

		// create and place all agents and their disks, add glue-EventHandler to all agentDisks
		for (int i = 0; i < agents.length; i++) {
			double scaleFactor = 1.0;
			controlledAgentStates[0].resetEnergyLevel(initialEnergyLevel);
			do {
				double posx = ScenarioUtils.uniform(eRandom, 0.0, getEnvironment().getMaxX());
				double posy = ScenarioUtils.uniform(eRandom, 0.0, getEnvironment().getMaxY());
				double orientation = 0.0;
				agentData[i] = agents[i].createControlledAgent(posx, posy, orientation, scaleFactor);
			} while (agentData[i] == null);

			for (int j = 0; j < agents[i].getSize(); j++) {
				agentDisks[i][j] = agentData[i].getDiskComplex().getDisks().get(j);
				
				// glue EventHandler for agent: objects always stick to agent
				Disk thisDisk = agentDisks[i][j];
				thisDisk.addEventHandler(new CollisionEventHandler() {
					@Override
					public void collision(CollidableObject otherDisk, Point collisionPoint, double exchangedImpulse) {
						if (otherDisk instanceof Disk) {
							getEnvironment().merge(thisDisk, (Disk) otherDisk);
						}
					}
				});
			}
		}

		// set all disks
		this.setDisks();

		return agentData;
	}

	/** to be implemented by inheriting scenario class */
	protected abstract void createAgents(Random envRandom);

	public void setAgents(Agent[] agents) {
		this.agents = agents;
	}
	public Disk[][] getAgentDisks(){
		return agentDisks;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	/* additional methods for initializing environment and agent */
	///////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * set all Disks in this scenario: should only be invoked once, when the scenario is created
	 */
	private void setDisks() {
		// initialize disks[]
		int sum = 0;
		for (int i = 0; i < agentDisks.length; i++) {
			sum += agentDisks[i].length;
		}
		sum += allDisksMapped.size();
		disks = new Disk[sum];
		
		int n = 0;
		// agentDisks
		for (int i = 0; i < agentDisks.length; i++) {
			for (int j = 0; j < agentDisks[i].length; j++) {
				disks[n] = agentDisks[i][j];
				n++;
			}
		}
		// allDisks
		for (Disk disk : allDisksMapped.keySet()) {
			disks[n] = disk;
			n++;
		}
		this.setDisks(disks);
	}
	
	
	/**
	 * generates floorCellTypes as HashMap with Color as key and corresponding FloorCellType as value.
	 * Is created only once for all seeds/instances of scenario to avoid reaching the limit of 256 floor types
	 */
	private HashMap<Double,FloorCellType> generateFloorCellTypes() {
		for (int i = 0; i < envObjectSet.size(); i++) {
			double colorIndex = envObjectSet.get(i)[0].getColorIndex();
			Color color = ColorIndexedDiskMaterial.getDisplayColorByIndex(colorIndex);
			FloorCellType valueFloorCellType = new FloorCellType(1.0, color);
			floorCellTypes.put(colorIndex, valueFloorCellType);
		}
		return floorCellTypes;
	}

	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/* called every time step, necessary update code (besides the DiskWorldPhysics) */
	///////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * for each object in bucket area with same color: move object into bucket center + reward
	 * if all objects are sorted: reward, reset, nextEpisode()
	 */
	@Override
	protected void updateAtEndOfTimeStep(AgentState[] agentStates) {

		// if all objects are sorted: reset all
		if (sortedItems == (numberOfTypes * numberOfObjectsPerType)) {
			agentStates[0].incEnergyGained(energyGainOnWin);
			resetAll();
			nextEpisode();
			sortedItems = 0;
		}

		// for each object check whether it is in the right bucket
		for (HashMap.Entry<Disk, EnvObject> element : allDisksMapped.entrySet()) {
		    Disk disk = element.getKey();
		    EnvObject envObject = element.getValue();
		    
		    // if not a bucket and not already sorted
		    if (!(envObject instanceof Bucket) && disk.getZLevel() == 0) {
		    	
		    	// object position rounded to hundred
		    	double objDiskXrounded = Math.floor(disk.getX() / 100);
				double objDiskYrounded = Math.floor(disk.getY() / 100);				
				double objectColorIndex = envObject.getColorIndex();

				double positionOfBucketInSameColorX = bucketPos.get(objectColorIndex).x;
				double positionOfBucketInSameColorY = bucketPos.get(objectColorIndex).y;
				
				if (positionOfBucketInSameColorX == objDiskXrounded && positionOfBucketInSameColorY == objDiskYrounded) {
					agentDisks[0][0].getDiskComplex().splitFromNeighbors(agentDisks[0][0]);					
					shiftIntoBucket(disk.getDiskComplex(), bucketPos.get(objectColorIndex));
					++sortedItems;
					agentStates[0].incEnergyGained(energyGainPerItem);
				}	
			}
		}
	}

	
	/**
	 * shift DiskComplex into associated bucket
	 * to avoid collision find a free ZLevel
	 */
	protected void shiftIntoBucket(DiskComplex shiftDisk, Point bucketPos) {
		int sortedZLevel = -3;
		
		// determine center of bucket as target
		double targetX = (bucketPos.getX() * 100) + Bucket.getRadius();
		double targetY = (bucketPos.getY() * 100) + Bucket.getRadius();
		
		boolean successfullyTeleported;
		do {
			// move on z-axis
			for (Disk disk : shiftDisk.getDisks()) {
				disk.setZLevel(sortedZLevel);
			}
			// try to teleport to center of bucket
			successfullyTeleported = getEnvironment().canTeleportCenterOfMass(shiftDisk, targetX, targetY, 0);
			// if not possible try in next level down
			sortedZLevel--;
		} while (!successfullyTeleported);
		
		shiftDisk.setDiskFixed(0);
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* reset environment */
	///////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * reset all elements of the environment after successful completion of task
	 * new positions for buckets, renew floor, reset positions of all disks (buckets and objects)
	 */
	private void resetAll() {
		// new coordinates for buckets
		generateBucketPositions();
		// reset floor
		paintFloor();

		/* 
		 * to avoid collision between not yet moved bucket and bucket that is being moved
		 * move each bucket to its own zLevel prior to teleportation
		 */
		int z = 2;
		for (HashMap.Entry<Disk, EnvObject> element : allDisksMapped.entrySet()) {
		    Disk disk = element.getKey();
		    EnvObject envObject = element.getValue();
		    if (envObject instanceof Bucket) {
		    	disk.setZLevel(z);
				z++;
			}
		}
		
		// reset all disks
		for (HashMap.Entry<Disk, EnvObject> element : allDisksMapped.entrySet()) {
		    Disk disk = element.getKey();
		    EnvObject envObject = element.getValue();
		    
		    // reset buckets
		    if (envObject instanceof Bucket) {
				double colorIndex = envObject.getColorIndex();
				double newPosX = (bucketPos.get(colorIndex).x * 100) + Bucket.getRadius()+1;
				double newPosY = (bucketPos.get(colorIndex).y * 100) + Bucket.getRadius()+1;
				// teleport to intended x,y position, then move down to bucketZLevel to avoid collision
				getEnvironment().canTeleportCenterOfMass(disk.getDiskComplex(), newPosX, newPosY, 0);
				disk.setZLevel(bucketZLevel);
			}
		    
		    // reset objects
		    else if (disk.getZLevel() != 0) { // if object is not already reseted
		    	disk.getDiskComplex().setDiskNotFixed(disk);
		    	disk.setZLevel(0);
				double posx,posy;
				do {
					// random position
					posx = ScenarioUtils.uniform(envRandom, 0.0, getEnvironment().getMaxX());
					posy = ScenarioUtils.uniform(envRandom, 0.0, getEnvironment().getMaxY());	
				} while(!getEnvironment().canTeleport(disk, posx, posy, 0));
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////////////////////
	/* methods used in initialize and resetAll */
	///////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * generate positions for buckets,
	 * store them in bucketPos mapped to color of the associated bucket
	 */
	private void generateBucketPositions() {
		bucketPos.clear();
		for (int i = 0; i < numberOfTypes; i++) {
			double nextColorIndex = envObjectSet.get(i)[0].getColorIndex();
			boolean isUnique;
			do {
				isUnique = true;
				int posx = ScenarioUtils.uniform(envRandom, 0, 9);
				int posy = ScenarioUtils.uniform(envRandom, 0, 9);
				Point newCoord = new Point(posx, posy);
				
				// check for duplicate in bucketPos
				Iterator<Point> it = bucketPos.values().iterator();
				Point point = null;
				while (it.hasNext()) {
					point = it.next();
					if (point.x == newCoord.x && point.y == newCoord.y) {
						isUnique = false;
					}
				}
				// if newCoord unique: store it mapped to nextColor
				if (isUnique) {
					bucketPos.put(nextColorIndex, newCoord);
				}
			} while(!isUnique);
		}
	}

	/**
	 * paintFloor with standard FloorCellType
	 * and add bucket areas in position and color corresponding to buckets if floorBucketsOn
	 */
	private void paintFloor() {
		Floor floor = getEnvironment().getFloor();
		floor.fill(FloorCellType.STONE);

		if (floorBucketsOn) {
			// integrate buckets in floor
			for (int i = 0; i < bucketPos.size(); i++) {
				double bucketColorIndex = envObjectSet.get(i)[0].getColorIndex();
				FloorCellType floorCellType = floorCellTypes.get(bucketColorIndex);
				
				int posx = (int) bucketPos.get(bucketColorIndex).x;
				int posy = (int) bucketPos.get(bucketColorIndex).y;
				
				floor.setType(posx, posy, floorCellType);
			}
		}
	}

}